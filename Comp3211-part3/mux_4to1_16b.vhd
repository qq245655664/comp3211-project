library ieee;
use ieee.std_logic_1164.all;

entity mux_4to1_16b is
    port (mux_select : in std_logic_vector(0 to 1);
          data_a     : in std_logic_vector(15 downto 0);
          data_b     : in std_logic_vector(15 downto 0);
          data_c     : in std_logic_vector(15 downto 0);
          data_d     : in std_logic_vector(15 downto 0);
          data_out   : out std_logic_vector(15 downto 0));
end mux_4to1_16b;

architecture structural of mux_4to1_16b is
begin

    data_out <= data_a after 0.2 ns when (mux_select = "00") else 
                data_b after 0.2 ns when (mux_select = "01") else 
                data_c after 0.2 ns when (mux_select = "10") else 
                data_d after 0.2 ns;                              
end structural;
