library ieee;
use ieee.std_logic_1164.all;
entity forwarding_unit is
    port ( mem_wb_reg_write : in std_logic;
           mem_wb_reg_rd    : in std_logic_vector(3 downto 0);
           ex_mem_reg_write : in std_logic;
           ex_mem_reg_rd    : in std_logic_vector(3 downto 0);
           id_ex_reg_rs     : in std_logic_vector(3 downto 0);
           id_ex_reg_rt     : in std_logic_vector(3 downto 0);
           id_ex_opcode     : in std_logic_vector(3 downto 0);
			  thread_id_ex     : in std_logic;
			  thread_ex_mem    : in std_logic;
			  thread_mem_wb    : in std_logic;
           sel_alu_src_a    : out std_logic_vector(0 to 1); -- 00:origin 01:mem_result 10:write_data
           sel_alu_src_b    : out std_logic_vector(0 to 1);
			  sel_store_val    : out std_logic_vector(0 to 1);
			  use_extend       : in std_logic);
end forwarding_unit;

architecture behavioural of forwarding_unit is
constant OP_ADDI  : std_logic_vector(3 downto 0) := "0100";
constant OP_SLL   : std_logic_vector(3 downto 0) := "0110";
constant OP_SRL   : std_logic_vector(3 downto 0) := "0111";
constant OP_LOAD  : std_logic_vector(3 downto 0) := "0010";
constant OP_LOADT : std_logic_vector(3 downto 0) := "0101";
constant OP_STORE : std_logic_vector(3 downto 0) := "0011";
constant OP_STORET: std_logic_vector(3 downto 0) := "1101";
signal buf_mem_wb_reg_rd : std_logic_vector(3 downto 0);
signal buf_ex_mem_reg_rd : std_logic_vector(3 downto 0);

begin
    buf_mem_wb_reg_rd <= mem_wb_reg_rd;
                         
    buf_ex_mem_reg_rd <= ex_mem_reg_rd;
          
    sel_alu_src_a <= "01" after 1.5 ns when (ex_mem_reg_write = '1')
                                        and (buf_ex_mem_reg_rd /= "0000")
                                        and (buf_ex_mem_reg_rd = id_ex_reg_rs) 
													 and (thread_ex_mem = thread_id_ex) else
                                        
                     "10" after 1.5 ns when (mem_wb_reg_write = '1')
                                        and (buf_mem_wb_reg_rd /= "0000")
                                        and (buf_ex_mem_reg_rd /= id_ex_reg_rs)
                                        and (buf_mem_wb_reg_rd = id_ex_reg_rs) 
													 and (thread_mem_wb = thread_id_ex) else
                     "00" after 1.5 ns;

    sel_alu_src_b <= "01" after 1.5 ns when (ex_mem_reg_write = '1')
                                        and (buf_ex_mem_reg_rd /= "0000")
                                        and (buf_ex_mem_reg_rd = id_ex_reg_rt) 
                                        and (id_ex_opcode /= OP_ADDI) 
                                        and (id_ex_opcode /= OP_SLL) 
                                        and (id_ex_opcode /= OP_SRL)
                                        and (id_ex_opcode /= OP_STORE) 
													 and (id_ex_opcode /= OP_STORET) 
													 and (thread_ex_mem = thread_id_ex) else
                                        
                     "10" after 1.5 ns when (mem_wb_reg_write = '1')
                                        and (mem_wb_reg_rd /= "0000")
                                        and (buf_ex_mem_reg_rd /= id_ex_reg_rt)
                                        and (buf_mem_wb_reg_rd = id_ex_reg_rt) 
                                        and (id_ex_opcode /= OP_ADDI) 
                                        and (id_ex_opcode /= OP_SLL) 
                                        and (id_ex_opcode /= OP_SRL)
                                        and (id_ex_opcode /= OP_STORE) 
													 and (id_ex_opcode /= OP_STORET) 
													 and (thread_mem_wb = thread_id_ex) else
						   "11" after 1.5 ns when use_extend = '1' else
                     "00" after 1.5 ns;
	
	 sel_store_val <= "01" after 1.5 ns when (ex_mem_reg_write = '1')
                                        and (buf_mem_wb_reg_rd /= "0000")
                                        and (buf_ex_mem_reg_rd = id_ex_reg_rt)
													 and (id_ex_opcode = OP_STORE 
                                        or   id_ex_opcode = OP_STORET) 
													 and (thread_ex_mem = thread_id_ex)else
						   
							"10" after 1.5 ns when (mem_wb_reg_write = '1')
                                        and (mem_wb_reg_rd /= "0000")
                                        and (buf_ex_mem_reg_rd /= id_ex_reg_rt)
                                        and (buf_mem_wb_reg_rd = id_ex_reg_rt)
                                        and (id_ex_opcode = OP_STORE 
                                        or   id_ex_opcode = OP_STORET) 
													 and (thread_mem_wb = thread_id_ex) else
                     "00" after 1.5 ns;													 
end behavioural;                        
