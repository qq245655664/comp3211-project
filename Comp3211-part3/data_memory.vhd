---------------------------------------------------------------------------
-- data_memory.vhd - Implementation of A Single-Port, 16 x 16-bit Data
--                   Memory.
-- 
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_TEXTIO.ALL;
use STD.TEXTIO.ALL;

entity data_memory is
    port ( reset        : in  std_logic;
           clk          : in  std_logic;
           write_enable : in  std_logic;
           write_data   : in  std_logic_vector(15 downto 0);
           addr_in      : in  std_logic_vector(8 downto 0);
           data_out     : out std_logic_vector(15 downto 0);
           test_finished: out std_logic);
end data_memory;

architecture behavioral of data_memory is

type mem_array is array(0 to 511) of std_logic_vector(15 downto 0);
signal sig_data_mem : mem_array := ((others => (others => '0')));

type test_array is array(0 to 255) of std_logic_vector(15 downto 0);
signal sig_data_compare : test_array := ((others => (others => '0')));
signal testResult : std_logic;

file data_file : text open read_mode is "data.txt";
file test_file : text open read_mode is "sol.txt";

begin
	 mem_process: process ( clk,
                           write_enable,
                           write_data,
                           addr_in ) is
  
    variable var_addr     : integer;	 
	 variable line_buf     : line;
	 variable data_buf     : std_logic_vector(15 downto 0);
    begin
        var_addr := conv_integer(addr_in);
        
        if (reset = '1') then
           -- writing data.txt from FILE to data memory
           for i in 0 to 511 loop
              if (not endfile(data_file)) then
                  readline(data_file,line_buf);
                  read(line_buf,data_buf);
                  sig_data_mem(i) <= data_buf;
              end if;
           end loop;
           -- writing sol.txt from FILE to test memory
           -- used for comparing with final data
           for i in 0 to 255 loop
              if (not endfile(test_file)) then
                  readline(test_file,line_buf);
                  read(line_buf,data_buf);
                  sig_data_compare(i) <= data_buf;
              end if;
           end loop;
        else
		     if (falling_edge(clk) and write_enable = '1') then
              -- memory writes on the rising clock edge
              sig_data_mem(var_addr) <= write_data after 28 ns;
			  end if;
        end if;
		  -- continuous read of the memory location given by var_addr 
        data_out <= sig_data_mem(var_addr) after 28 ns;
        
        -- generate the test result
        testResult <= '1';
        for i in 0 to 255 loop
            if (sig_data_mem(i) /= sig_data_compare(i)) then
               testResult <= '0';
            end if;
        end loop;
        test_finished <= testResult;
    end process;
end behavioral;
