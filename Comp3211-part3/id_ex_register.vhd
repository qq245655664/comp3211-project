library ieee;
use ieee.std_logic_1164.all;

entity id_ex_register is
   port(reset                 : in std_logic;
		clk                     : in std_logic;
		alu_src_id              : in std_logic;
		alu_ctl_id              : in std_logic_vector(2 downto 0);
		tag_write_id            : in std_logic;
		mem_write_id            : in std_logic;
		tag_to_reg_id           : in std_logic;
		mem_to_reg_id           : in std_logic;
		jump_condition_id		   : in std_logic;
		pc_to_pc_branch_id      : in std_logic;
		is_bez_id               : in std_logic;
		reg_write_id			   : in std_logic;
		read_data_a_id          : in std_logic_vector(15 downto 0);
		read_data_b_id          : in std_logic_vector(15 downto 0);
		sign_extended_offset_id : in std_logic_vector(15 downto 0);
		read_reg_a_id			   : in std_logic_vector(3 downto 0);
		read_reg_b_id			   : in std_logic_vector(3 downto 0);
		write_reg_id			   : in std_logic_vector(3 downto 0);
		op_code_id				   : in std_logic_vector(3 downto 0);
		branch_addr_id			   : in std_logic_vector(7 downto 0);
		flush                   : in std_logic;
		stall                   : in std_logic;
		thread_id               : in std_logic;
		alu_src_ex              : out std_logic;
		alu_ctl_ex              : out std_logic_vector(2 downto 0);
		tag_write_ex            : out std_logic;
		mem_write_ex            : out std_logic;
		tag_to_reg_ex           : out std_logic;
		mem_to_reg_ex           : out std_logic;
		jump_condition_ex		   : out std_logic;
		pc_to_pc_branch_ex      : out std_logic;
		is_bez_ex               : out std_logic;
		reg_write_ex			   : out std_logic;
		read_data_a_ex          : out std_logic_vector(15 downto 0);
		read_data_b_ex          : out std_logic_vector(15 downto 0);
		sign_extended_offset_ex : out std_logic_vector(15 downto 0);
		read_reg_a_ex			   : out std_logic_vector(3 downto 0);
		read_reg_b_ex			   : out std_logic_vector(3 downto 0);
		write_reg_ex			   : out std_logic_vector(3 downto 0);
		op_code_ex				   : out std_logic_vector(3 downto 0);
		branch_addr_ex			   : out std_logic_vector(7 downto 0);
		thread_ex               : out std_logic);
end id_ex_register;

architecture behaviour of id_ex_register is
begin
   process(reset, clk) is
	begin
      if (reset = '1') then
         alu_src_ex <= '0';
         alu_ctl_ex <= (others => '0');
         tag_write_ex <= '0';
         mem_write_ex <= '0';
         tag_to_reg_ex <= '0';
         mem_to_reg_ex <= '0';
         jump_condition_ex <= '0';
         pc_to_pc_branch_ex <= '0';
         is_bez_ex <= '0';
		   reg_write_ex <= '0';
		   read_data_a_ex <= (others => '0');
         read_data_b_ex <= (others => '0');
         sign_extended_offset_ex <= (others => '0');
		   read_reg_a_ex <= (others => '0');
		   read_reg_b_ex <= (others => '0');
		   write_reg_ex <= (others => '0');
		   op_code_ex <= (others => '0');
		   branch_addr_ex <= (others => '0');
      elsif (falling_edge(clk)) then
		   if (stall = '1') then
			   -- do nothing
		   elsif (flush = '0') then
				alu_src_ex         <= alu_src_id after 0.5 ns;
				alu_ctl_ex         <= alu_ctl_id after 0.5 ns;
				tag_write_ex       <= tag_write_id after 0.5 ns;
				mem_write_ex       <= mem_write_id after 0.5 ns;
				tag_to_reg_ex      <= tag_to_reg_id after 0.5 ns;
				mem_to_reg_ex      <= mem_to_reg_id after 0.5 ns;
				jump_condition_ex  <= jump_condition_id after 0.5 ns;
				pc_to_pc_branch_ex <= pc_to_pc_branch_id after 0.5 ns;
				is_bez_ex          <= is_bez_id after 0.5 ns;
				reg_write_ex       <= reg_write_id after 0.5 ns;
				read_data_a_ex     <= read_data_a_id after 0.5 ns;
				read_data_b_ex     <= read_data_b_id after 0.5 ns;
				sign_extended_offset_ex <= sign_extended_offset_id after 0.5 ns;
				read_reg_a_ex      <= read_reg_a_id after 0.5 ns;
				read_reg_b_ex      <= read_reg_b_id after 0.5 ns;
				write_reg_ex       <= write_reg_id after 0.5 ns;
				op_code_ex         <= op_code_id after 0.5 ns;
				branch_addr_ex     <= branch_addr_id after 0.5 ns;
				thread_ex          <= thread_id after 0.5 ns;
			else
			   alu_src_ex <= '0';
				alu_ctl_ex <= (others => '0');
				tag_write_ex <= '0';
				mem_write_ex <= '0';
				tag_to_reg_ex <= '0';
				mem_to_reg_ex <= '0';
				jump_condition_ex <= '0';
				pc_to_pc_branch_ex <= '0';
				is_bez_ex <= '0';
				reg_write_ex <= '0';
				read_data_a_ex <= (others => '0');
				read_data_b_ex <= (others => '0');
				sign_extended_offset_ex <= (others => '0');
				read_reg_a_ex <= (others => '0');
				read_reg_b_ex <= (others => '0');
				write_reg_ex <= (others => '0');
				op_code_ex <= (others => '0');
				branch_addr_ex <= (others => '0');		
			end if;
      end if;
   end process;
end behaviour;
