library ieee;
use ieee.std_logic_1164.all;

entity if_id_register is
   port( reset      : in std_logic;
         clk        : in std_logic;
         insn_if    : in std_logic_vector(15 downto 0);
			flush      : in std_logic;
			stall      : in std_logic;
			thread_if  : in std_logic;
         insn_id    : out std_logic_vector(15 downto 0);
			thread_id  : out std_logic);
end if_id_register;

architecture behaviour of if_id_register is
begin
   process(reset, clk) is
	begin
      --On falling edge, update the values in the register.
      if (reset = '1') then
         insn_id <= (others => '0');
      elsif (falling_edge(clk)) then
		   if (stall = '1') then
			-- do nothing	
		   elsif (flush = '0') then
			    thread_id <= thread_if after 0.5 ns;
     		    insn_id <= insn_if after 0.5 ns;
		   else
			    insn_id <= (others => '0');						 
			end if;
      end if;
   end process;
end behaviour;
