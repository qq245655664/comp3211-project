---------------------------------------------------------------------------
-- control_unit.vhd - Control Unit Implementation
--
-- Notes: refer to headers in single_cycle_core.vhd for the supported ISA.
--
--  control signals:
--     reg_dst    : asserted for ADD instructions, so that the register
--                  destination number for the 'write_register' comes from
--                  the rd field (bits 3-0). 
--     reg_write  : asserted for ADD and LOAD instructions, so that the
--                  register on the 'write_register' input is written with
--                  the value on the 'write_data' port.
--     alu_src    : asserted for LOAD and STORE instructions, so that the
--                  second ALU operand is the sign-extended, lower 4 bits
--                  of the instruction.
--     mem_write  : asserted for STORE instructions, so that the data 
--                  memory contents designated by the address input are
--                  replaced by the value on the 'write_data' input.
--     mem_to_reg : asserted for LOAD instructions, so that the value fed
--                  to the register 'write_data' input comes from the
--                  data memory.
--
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity control_unit is
    port ( opcode          : in  std_logic_vector(3 downto 0);
           ext_src         : out std_logic;
           reg_src         : out std_logic;
           reg_dst         : out std_logic;
           reg_write       : out std_logic;
           alu_src         : out std_logic;
			  alu_ctl         : out std_logic_vector(0 to 2);
           mem_write       : out std_logic;
           tag_write       : out std_logic;
           mem_to_reg      : out std_logic;
           tag_to_reg      : out std_logic;
			  pc_to_pc_jump   : out std_logic;
			  pc_to_pc_branch : out std_logic;
			  is_bez          : out std_logic);
end control_unit;

architecture behavioural of control_unit is

constant OP_LOAD  : std_logic_vector(3 downto 0) := "0010";
constant OP_STORE : std_logic_vector(3 downto 0) := "0011";
constant OP_ADD   : std_logic_vector(3 downto 0) := "1000";
constant OP_SUB   : std_logic_vector(3 downto 0) := "1001";
constant OP_SLT   : std_logic_vector(3 downto 0) := "0001";
constant OP_AND   : std_logic_vector(3 downto 0) := "1010";
constant OP_OR    : std_logic_vector(3 downto 0) := "1011";
constant OP_ADDI  : std_logic_vector(3 downto 0) := "0100";
constant OP_SLL   : std_logic_vector(3 downto 0) := "0110";
constant OP_SRL   : std_logic_vector(3 downto 0) := "0111";
constant OP_BEZ   : std_logic_vector(3 downto 0) := "1110";
constant OP_BNZ   : std_logic_vector(3 downto 0) := "1111";
constant OP_JUMP  : std_logic_vector(3 downto 0) := "1100";
constant OP_LOADT : std_logic_vector(3 downto 0) := "0101";
constant OP_STORET: std_logic_vector(3 downto 0) := "1101";

constant ALU_ADD : std_logic_vector(0 to 2) := "000";
constant ALU_SUB : std_logic_vector(0 to 2) := "001";
constant ALU_AND : std_logic_vector(0 to 2) := "010";
constant ALU_OR  : std_logic_vector(0 to 2) := "011";
constant ALU_SLL : std_logic_vector(0 to 2) := "100";
constant ALU_SRL : std_logic_vector(0 to 2) := "101";
constant ALU_SLT : std_logic_vector(0 to 2) := "110";
constant ALU_DEF : std_logic_vector(0 to 2) := "111";

begin
    ext_src         <= '1' after 1.5 ns when (opcode = OP_ADDI) else
                       '0' after 1.5 ns;
    
    reg_src         <= '0' after 1.5 ns when (opcode = OP_ADDI) else
                       '1' after 1.5 ns;
    
    reg_dst         <= '1' after 1.5 ns when (opcode = OP_ADD 
                              or opcode = OP_SUB
							  	      or opcode = OP_SLT
								      or opcode = OP_AND
							   	   or opcode = OP_OR) else
                       '0' after 1.5 ns;

    reg_write       <= '1' after 1.5 ns when (opcode = OP_ADD
                              or opcode = OP_ADDI
	                           or opcode = OP_SUB
				  	   		   	or opcode = OP_SLT
			  				     	   or opcode = OP_AND
		  				   		   or opcode = OP_OR
                              or opcode = OP_SLL
                              or opcode = OP_SRL
                              or opcode = OP_LOAD
                              or opcode = OP_LOADT) else
                       '0' after 1.5 ns;
    
    alu_src         <= '1' after 1.5 ns when (opcode = OP_ADDI
                              or opcode = OP_SLL
                              or opcode = OP_SRL
                              or opcode = OP_LOAD
                              or opcode = OP_LOADT                         
                              or opcode = OP_STORE
                              or opcode = OP_STORET
										or opcode = OP_JUMP) else
                       '0' after 1.5 ns;
						
    alu_ctl         <= ALU_ADD after 1.5 ns when (opcode = OP_ADD) else
	                    ALU_SUB after 1.5 ns when (opcode = OP_SUB) else
						     ALU_AND after 1.5 ns when (opcode = OP_AND) else
						     ALU_OR  after 1.5 ns when (opcode = OP_OR ) else
						     ALU_SLL after 1.5 ns when (opcode = OP_SLL) else
						     ALU_SRL after 1.5 ns when (opcode = OP_SRL) else
						     ALU_SLT after 1.5 ns when (opcode = OP_SLT) else
							  ALU_ADD after 1.5 ns when (opcode = OP_JUMP)else
						     ALU_DEF after 1.5 ns;
                 
    mem_write       <= '1' after 1.5 ns when opcode = OP_STORE else
                       '0' after 1.5 ns;
                 
    mem_to_reg      <= '1' after 1.5 ns when opcode = OP_LOAD else
                       '0' after 1.5 ns;
    
    tag_write       <= '1' after 1.5 ns when opcode = OP_STORET else
                       '0' after 1.5 ns;
                  
    tag_to_reg      <= '1' after 1.5 ns when opcode = OP_LOADT else
                       '0' after 1.5 ns;

	 pc_to_pc_jump   <= '1' after 1.5 ns when opcode = OP_JUMP else
	                    '0' after 1.5 ns;
	 
	 pc_to_pc_branch <= '1' after 1.5 ns when (opcode = OP_BEZ 
	                         or   opcode = op_BNZ) else
							  '0' after 1.5 ns;
				 
				 is_bez <= '1' after 1.5 ns when opcode = OP_BEZ else
				           '0' after 1.5 ns;
end behavioural;
