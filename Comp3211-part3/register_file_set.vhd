---------------------------------------------------------------------------
-- register_file.vhd - Implementation of A Dual-Port, 16 x 16-bit
--                     Collection of Registers.
-- 
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity register_file_set is
    port ( reset           : in  std_logic;
           clk             : in  std_logic;
           read_register_a : in  std_logic_vector(3 downto 0);
           read_register_b : in  std_logic_vector(3 downto 0);
           write_enable    : in  std_logic;
           write_register  : in  std_logic_vector(3 downto 0);
           write_data      : in  std_logic_vector(15 downto 0);
			  thread_read     : in  std_logic;
			  thread_write    : in  std_logic;
           read_data_a     : out std_logic_vector(15 downto 0);
           read_data_b     : out std_logic_vector(15 downto 0));
end register_file_set;

architecture behavioral of register_file_set is

type reg_file is array(0 to 15) of std_logic_vector(15 downto 0);
signal sig_regfile_thread1 : reg_file;
signal sig_regfile_thread2 : reg_file;

begin

    mem_process : process ( reset,
                            clk,
                            read_register_a,
                            read_register_b,
                            write_enable,
                            write_register,
                            write_data ) is

    variable var_read_addr_a : integer;
    variable var_read_addr_b : integer;
    variable var_write_addr  : integer;
    
    begin
    
        var_read_addr_a := conv_integer(read_register_a);
        var_read_addr_b := conv_integer(read_register_b);
        var_write_addr  := conv_integer(write_register);
        
        -- continuous read of the registers at location read_register_a
        -- and read_register_b
		  if (thread_read = '0') then
			  read_data_a <= sig_regfile_thread1(var_read_addr_a) after 2 ns; 
			  read_data_b <= sig_regfile_thread1(var_read_addr_b) after 2 ns;
		  else
			  read_data_a <= sig_regfile_thread2(var_read_addr_a) after 2 ns; 
			  read_data_b <= sig_regfile_thread2(var_read_addr_b) after 2 ns;
        end if;			
		  
		  -- initialize and write operation
		  if (reset = '1') then
				-- initial values of the registers - reset to zeroes
				sig_regfile_thread1 <= ((others => (others => '0')));
				sig_regfile_thread2 <= ((others => (others => '0')));
		  elsif (rising_edge(clk) and write_enable = '1') then
				-- register write on the falling clock edge
				if (thread_write = '0') then --thread 1
					sig_regfile_thread1(var_write_addr) <= write_data after 2 ns;
					if (read_register_a = write_register) then
						 read_data_a <= write_data after 2 ns; 
					end if;
					if (read_register_b = write_register) then
						 read_data_b <= write_data after 2 ns; 
					end if;
				else -- thread 2
					sig_regfile_thread2(var_write_addr) <= write_data after 2 ns;
					if (read_register_a = write_register) then
						 read_data_a <= write_data after 2 ns; 
					end if;
					if (read_register_b = write_register) then
						 read_data_b <= write_data after 2 ns; 
					end if;			
				end if;
		  end if;
		  

        -- enforces value zero for register $0
        sig_regfile_thread1(0) <= X"0000";
		  sig_regfile_thread2(0) <= X"0000";
       

    end process; 
end behavioral;
