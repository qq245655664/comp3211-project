----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:51:26 05/19/2016 
-- Design Name: 
-- Module Name:    mux_thread_mem - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_thread_mem is
port(
	    tag_write_mem_thread1       : in std_logic;
		 tag_write_mem_thread2       : in std_logic;
       mem_write_mem_thread1       : in std_logic;
		 mem_write_mem_thread2       : in std_logic;
       tag_to_reg_mem_thread1      : in std_logic;
		 tag_to_reg_mem_thread2      : in std_logic;
		 mem_to_reg_mem_thread1      : in std_logic;
       mem_to_reg_mem_thread2      : in std_logic;
       jump_condition_mem_thread1  : in std_logic;
		 jump_condition_mem_thread2  : in std_logic;
       pc_to_pc_branch_mem_thread1 : in std_logic;
		 pc_to_pc_branch_mem_thread2 : in std_logic;
       is_bez_mem_thread1          : in std_logic;--
		 is_bez_mem_thread2          : in std_logic;--
	    reg_write_mem_thread1	     : in std_logic;
		 reg_write_mem_thread2	     : in std_logic;
	    zero_mem_thread1			     : in std_logic;--
		 zero_mem_thread2			     : in std_logic;--
	    alu_result_mem_thread1      : in std_logic_vector(15 downto 0);
		 alu_result_mem_thread2      : in std_logic_vector(15 downto 0);
       read_data_b_mem_thread1     : in std_logic_vector(15 downto 0);
		 read_data_b_mem_thread2     : in std_logic_vector(15 downto 0);
       write_reg_mem_thread1       : in std_logic_vector(3 downto 0);
       write_reg_mem_thread2       : in std_logic_vector(3 downto 0);
	    branch_addr_mem_thread1     : in std_logic_vector(7 downto 0);
		 branch_addr_mem_thread2     : in std_logic_vector(7 downto 0);
       op_code_mem_thread1         : in std_logic_vector(3 downto 0);
		 op_code_mem_thread2         : in std_logic_vector(3 downto 0);
		 thread_mem_thread1          : in std_logic;
		 thread_mem_thread2          : in std_logic;
		 thread_using_mem            : in std_logic;
		 tag_write_mem               : out std_logic;
       mem_write_mem               : out std_logic;
       tag_to_reg_mem              : out std_logic;
       mem_to_reg_mem              : out std_logic;
       jump_condition_mem          : out std_logic;
       pc_to_pc_branch_mem         : out std_logic;
       is_bez_mem                  : out std_logic;
	    reg_write_mem	              : out std_logic;
	    zero_mem			           : out std_logic;
	    alu_result_mem              : out std_logic_vector(15 downto 0);
       read_data_b_mem             : out std_logic_vector(15 downto 0);
       write_reg_mem               : out std_logic_vector(3 downto 0);
	    branch_addr_mem	           : out std_logic_vector(7 downto 0);--
       op_code_mem                 : out std_logic_vector(3 downto 0);
		 thread_mem                  : out std_logic;
		 addr                        : out std_logic_vector(15 downto 0));
end mux_thread_mem;

architecture Behavioral of mux_thread_mem is

begin
       --signals that must be kept stable for memory to read
		 tag_write_mem       <= tag_write_mem_thread1 when thread_using_mem = '0' else tag_write_mem_thread2 after 0.2 ns;    
       mem_write_mem       <= mem_write_mem_thread1 when thread_using_mem = '0' else mem_write_mem_thread2 after 0.2 ns;   
       read_data_b_mem     <= read_data_b_mem_thread1 when thread_using_mem = '0' else read_data_b_mem_thread2 after 0.2 ns;
       addr                <=  alu_result_mem_thread1 when thread_using_mem = '0' else alu_result_mem_thread2 after 0.2 ns;		 
       
		 --signals that don't need to be used by memory
		 tag_to_reg_mem      <= tag_to_reg_mem_thread2 when thread_using_mem = '0' else tag_to_reg_mem_thread1 after 0.2 ns;              
       mem_to_reg_mem      <= mem_to_reg_mem_thread2 when thread_using_mem = '0' else mem_to_reg_mem_thread1 after 0.2 ns;		 
       jump_condition_mem  <= jump_condition_mem_thread2 when thread_using_mem = '0' else jump_condition_mem_thread1 after 0.2 ns;         
       pc_to_pc_branch_mem <= pc_to_pc_branch_mem_thread2 when thread_using_mem = '0' else pc_to_pc_branch_mem_thread1 after 0.2 ns;		 
       is_bez_mem          <= is_bez_mem_thread2 when thread_using_mem = '0' else is_bez_mem_thread1 after 0.2 ns;                 
	    reg_write_mem	      <= reg_write_mem_thread2 when thread_using_mem = '0' else reg_write_mem_thread1 after 0.2 ns;             
	    zero_mem            <= zero_mem_thread2 when thread_using_mem = '0' else zero_mem_thread1 after 0.2 ns;			          
	    alu_result_mem      <= alu_result_mem_thread2 when thread_using_mem = '0' else alu_result_mem_thread1 after 0.2 ns;                      
       write_reg_mem       <= write_reg_mem_thread2 when thread_using_mem = '0' else write_reg_mem_thread1 after 0.2 ns;        
	    branch_addr_mem	   <= branch_addr_mem_thread2 when thread_using_mem = '0' else branch_addr_mem_thread1 after 0.2 ns;        
       op_code_mem         <= op_code_mem_thread2 when thread_using_mem = '0' else op_code_mem_thread1 after 0.2 ns;        
		 thread_mem          <= thread_mem_thread2 when thread_using_mem = '0' else thread_mem_thread1 after 0.2 ns;        

end Behavioral;

