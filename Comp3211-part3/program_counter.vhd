---------------------------------------------------------------------------
-- program_counter.vhd - Program Counter Implementation 
-- 
-- Note : The program counter is simply a register that updates its output 
-- on the rising clock edge.
-- 
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Word = 16-bit
-- Address Indexing = 16-bit
-- Offset = -8 ~ +7
-- Address Range = 0 ~ 255 (0x000 ~ 0x0FF)

entity program_counter is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(7 downto 0);
           addr_out : out std_logic_vector(7 downto 0);
           stall    : in  std_logic;
			  update   : in  std_logic;
			  pc_initialize : in std_logic_vector(7 downto 0);
			  flush_pc : in  std_logic);
end program_counter;

architecture behavioral of program_counter is
signal buf_pc: std_logic_vector(7 downto 0);
begin

    update_process: process ( reset, 
                              clk ) is
    begin
       if (reset = '1') then
           addr_out <= pc_initialize; 
       elsif (falling_edge(clk)) then
		     if (flush_pc = '1') then
			     if (stall = '1') then
				     --flush due to state changing from two thread accessing memory to one thread only decrease counter by 1
					  addr_out <= buf_pc - "00000001" after 0.5 ns;
			     else
				     --flush due to state changing from no thread accessing memory to one thread decrease counter by 2
				     addr_out <= buf_pc - "00000010" after 0.5 ns;
              end if;			  
 			  elsif (stall = '0' and update ='1') then
			        --stall is not set and update is set, this counter is working normally
			     buf_pc <= addr_in;
              addr_out <= addr_in after 0.5 ns;  
		     end if;
		 end if;
    end process;
end behavioral;
