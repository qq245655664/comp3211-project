library ieee;
use ieee.std_logic_1164.all;

entity ex_mem_register is
   port(reset              : in std_logic;
       clk                 : in std_logic;
       tag_write_ex        : in std_logic;
       mem_write_ex        : in std_logic;
       tag_to_reg_ex       : in std_logic;
       mem_to_reg_ex       : in std_logic;
       jump_condition_ex   : in std_logic;
       pc_to_pc_branch_ex  : in std_logic;
       is_bez_ex           : in std_logic;
	    reg_write_ex		   : in std_logic;
	    zero_ex			      : in std_logic;
	    alu_result_ex       : in std_logic_vector(15 downto 0);
       read_data_b_ex      : in std_logic_vector(15 downto 0);
       write_reg_ex        : in std_logic_vector(3 downto 0);
	    branch_addr_ex	   : in std_logic_vector(7 downto 0);
       op_code_ex          : in std_logic_vector(3 downto 0);
		 flush               : in std_logic;
		 stall               : in std_logic;
		 thread_ex           : in std_logic;
		 update              : in std_logic;
		 thread_initialize   : in std_logic;
       tag_write_mem       : out std_logic;
       mem_write_mem       : out std_logic;
       tag_to_reg_mem      : out std_logic;
       mem_to_reg_mem      : out std_logic;
       jump_condition_mem  : out std_logic;
       pc_to_pc_branch_mem : out std_logic;
       is_bez_mem          : out std_logic;
	    reg_write_mem	      : out std_logic;
	    zero_mem			   : out std_logic;
	    alu_result_mem      : out std_logic_vector(15 downto 0);
       read_data_b_mem     : out std_logic_vector(15 downto 0);
       write_reg_mem       : out std_logic_vector(3 downto 0);
	    branch_addr_mem	   : out std_logic_vector(7 downto 0);
       op_code_mem         : out std_logic_vector(3 downto 0);
		 thread_mem          : out std_logic);
end ex_mem_register;

architecture behaviour of ex_mem_register is
begin
   process(reset, clk) is
	begin
      if (reset = '1') then
			tag_write_mem <= '0';
			mem_write_mem <= '0';
			tag_to_reg_mem <= '0';
			mem_to_reg_mem <= '0';
			jump_condition_mem <= '0';
			pc_to_pc_branch_mem <= '0';
			is_bez_mem <= '0';
			reg_write_mem <= '0';
			zero_mem <= '0';
			alu_result_mem <= (others => '0');
			read_data_b_mem <= (others => '0');
			write_reg_mem <= (others => '0');
			branch_addr_mem <= (others => '0');
			op_code_mem <= (others => '0');
			thread_mem <= thread_initialize;
      elsif (falling_edge(clk)) then
		   if (stall = '1') then
			   -- do nothing
			elsif (flush = '0') then
			   if ( update = '1') then
					tag_write_mem <= tag_write_ex after 0.5 ns;
					mem_write_mem <= mem_write_ex after 0.5 ns;
					tag_to_reg_mem <= tag_to_reg_ex after 0.5 ns;
					mem_to_reg_mem <= mem_to_reg_ex after 0.5 ns;
					jump_condition_mem <= jump_condition_ex after 0.5 ns;
					pc_to_pc_branch_mem <= pc_to_pc_branch_ex after 0.5 ns;
					is_bez_mem <= is_bez_ex after 0.5 ns;
					reg_write_mem <= reg_write_ex after 0.5 ns;
					zero_mem <= zero_ex after 0.5 ns;
					alu_result_mem <= alu_result_ex after 0.5 ns;
					read_data_b_mem <= read_data_b_ex after 0.5 ns;
					write_reg_mem <= write_reg_ex after 0.5 ns;
					branch_addr_mem <= branch_addr_ex after 0.5 ns;
					op_code_mem <= op_code_ex after 0.5 ns;
					thread_mem  <= thread_ex after 0.5 ns;
				end if;
			else
			   -- flush all signals
				tag_write_mem <= '0' after 0.5 ns;
				mem_write_mem <= '0' after 0.5 ns;
				tag_to_reg_mem <= '0' after 0.5 ns;
				mem_to_reg_mem <= '0' after 0.5 ns;
				jump_condition_mem <= '0' after 0.5 ns;
				pc_to_pc_branch_mem <= '0' after 0.5 ns;
				is_bez_mem <= '0' after 0.5 ns;
				reg_write_mem <= '0' after 0.5 ns;
				zero_mem <= '0' after 0.5 ns;
				alu_result_mem <= (others => '0') after 0.5 ns;
				read_data_b_mem <= (others => '0') after 0.5 ns;
				write_reg_mem <= (others => '0') after 0.5 ns;
				branch_addr_mem <= (others => '0') after 0.5 ns;
				op_code_mem <= (others => '0') after 0.5 ns;
			end if;			
      end if;
   end process;
end behaviour;