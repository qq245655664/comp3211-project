----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:40:07 04/27/2016 
-- Design Name: 
-- Module Name:    stall_units - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity branching_switching_unit is
    Port (op_code_mem     :  in std_logic_vector(3 downto 0);
		    branch_condition:  in std_logic;
		    op_code_ex      :  in std_logic_vector(3 downto 0);
		    op_code_id      :  in std_logic_vector(3 downto 0);
			 clk             :  in std_logic;
			 reset           :  in std_logic;
		    branch_flush    :  out std_logic;
		    stall           :  out std_logic;
			 curr_thread     :  out std_logic;
			 switch_flush    :  out std_logic);
end branching_switching_unit;

architecture Behavioral of branching_switching_unit is
constant OP_LOAD  : std_logic_vector(3 downto 0) := "0010";
constant OP_LOADT : std_logic_vector(3 downto 0) := "0101";
constant OP_STORE : std_logic_vector(3 downto 0) := "0011";
constant OP_STORET: std_logic_vector(3 downto 0) := "1101";
constant OP_BEZ   : std_logic_vector(3 downto 0) := "1110";
constant OP_BNZ   : std_logic_vector(3 downto 0) := "1111";
constant OP_ADDI  : std_logic_vector(3 downto 0) := "0100";
constant OP_JUMP  : std_logic_vector(3 downto 0) := "1100";
-- Memory Access Clock Cycle
constant MACC     : std_logic_vector(3 downto 0) := "0110";
signal mem_timer  : std_logic_vector (3 downto 0):= MACC;

--Buffer signals that keep track of the state
signal buf_switch_thread : std_logic;
signal buf_branch_flush : std_logic;
signal buf_curr_thread : std_logic;
signal buf_stall : std_logic;


begin
    --output signal
   branch_flush <= buf_branch_flush;
	curr_thread <= buf_curr_thread;
	switch_flush <= buf_switch_thread;
   stall <= buf_stall;

	buf_branch_flush <= '1' when (op_code_mem = OP_JUMP) 
	                        or ((op_code_mem = OP_BEZ or op_code_mem = OP_BNZ) and (branch_condition = '1'))else
				'0' after 1.5ns;
	
	stall_mem_process: process (clk) is
	begin
		if (reset = '1') then
			mem_timer <= MACC;
			buf_switch_thread <= '0';
			buf_curr_thread <= '0';
			buf_stall <= '0';
		elsif (falling_edge(clk)) then
			buf_switch_thread <= '0';
			if (mem_timer = "0000") then				
				if (buf_stall = '1') then   --change from two thread accessing memory to one thread accessing memory
					buf_stall <= '0' after 1.5 ns;
					mem_timer <= MACC - 1;					
					buf_curr_thread <= not buf_curr_thread after 1.5 ns;
					buf_switch_thread <= '1' after 1.5 ns;
				else                        --change from one thread accessing mermory to no thread accessing memory
					mem_timer <= MACC;
				end if;
			elsif (mem_timer /= MACC) then --change from one thread accessing memory to two thread acessing memory
				mem_timer <= mem_timer - 1;
				if (op_code_ex = OP_STORE or
				    op_code_ex = OP_STORET or
					 op_code_ex = OP_LOAD or
					 op_code_ex = OP_LOADT) and
					 buf_switch_thread = '0' and
					 buf_branch_flush = '0' then
					 buf_stall <= '1' after 1.5 ns;
				end if;
			else                            -- change from no thread accessing memory to one thread accessing memory
			   if (op_code_ex = OP_STORE or
				    op_code_ex = OP_STORET or
					 op_code_ex = OP_LOAD or
					 op_code_ex = OP_LOADT) and
					 buf_branch_flush = '0' then -- if branch flush is triggered, don't need to switch_thread
					mem_timer <= mem_timer - 1;
					buf_curr_thread <= not buf_curr_thread after 1.5 ns;
					buf_switch_thread <= '1' after 1.5 ns;
				end if;
			end if;		
		end if;
	end process;												
end Behavioral;