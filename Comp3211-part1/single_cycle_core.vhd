---------------------------------------------------------------------------
-- single_cycle_core.vhd - A Single-Cycle Processor Implementation
--
-- Notes : 
--
-- See single_cycle_core.pdf for the block diagram of this single
-- cycle processor core.
--
-- Instruction Set Architecture (ISA) for the single-cycle-core:
--   Each instruction is 16-bit wide, with four 4-bit fields.

-- noop      
--    # no operation or to signal end of program
--    # format:  | opcode = 0 |  0   |  0   |   0    |

-- slt   rd, rs, rt
--    # rd <- (rs < rt) ? 1 : 0
--    # format:  | opcode = 1 |  rs  |  rt  |   rd   |

-- load  rt, rs, offset     
--    # load data at memory location (rs + offset) into rt
--    # format:  | opcode = 2 |  rs  |  rt  | offset |

-- store rt, rs, offset
--    # store data rt into memory location (rs + offset)
--    # format:  | opcode = 3 |  rs  |  rt  | offset |

-- addi  rs, $immediate
--    # rs <- rs + $immediate
--    # format:  | opcode = 4 |  rs  |  $immediate   |

-- loadt rt, rs, offset     
--    # load tag at tagmem location (rs + offset) into rt
--    # format:  | opcode = 5 |  rs  |  rt  | offset |

-- sll   rt, rs, shamt
--    # rt <- rs << Shamt
--    # format:  | opcode = 6 |  rs  |  rt  | shamt |

-- srl   rt, rs, shamt
--    # rt <- rs >> Shamt
--    # format:  | opcode = 7 |  rs  |  rt  | shamt |

-- add   rd, rs, rt
--    # rd <- rs + rt
--    # format:  | opcode = 8 |  rs  |  rt  |   rd   |

-- sub   rd, rs, rt
--    # rd <- rs - rt
--    # format:  | opcode = 9 |  rs  |  rt  |   rd   | 
--        
-- and   rd, rs, rt
--    # rd <- rs & rt
--    # format:  | opcode = A |  rs  |  rt  |   rd   |

-- or    rd, rs, rt
--    # rd <- rs | rt
--    # format:  | opcode = B |  rs  |  rt  |   rd   |

-- jump  rs, offset
--    # PC = rs + offset
--    # format:  | opcode = C |  rs  | 0000 | offset |

-- storet rt, rs, offset
--    # store tag rt into tag_memory location (rs + offset)
--    # format:  | opcode = D |  rs  |  rt  | offset |

-- bez   rt, $branchAddr
--    # if (rs == 0) { PC = PC + 1 + $branchAddr }
--    # format:  | opcode = E |  rt  |  $branchAddr  |

-- bnz   rt, $branchAddr
--    # if (rs != 0) { PC = PC + 1 + $branchAddr }
--    # format:  | opcode = F |  rt  |  $branchAddr  |

-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity single_cycle_core is
    port ( reset  : in  std_logic;
           clk    : in  std_logic );
end single_cycle_core;

architecture structural of single_cycle_core is

component program_counter is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(7 downto 0);
           addr_out : out std_logic_vector(7 downto 0) );
end component;

component instruction_memory is
    port ( reset    : in  std_logic;
           addr_in  : in  std_logic_vector(7 downto 0);
           insn_out : out std_logic_vector(15 downto 0) );
end component;

component sign_extend_4to16 is
    port ( data_in  : in  std_logic_vector(3 downto 0);
           data_out : out std_logic_vector(15 downto 0) );
end component;

component sign_extend_8to16 is
    port ( data_in  : in  std_logic_vector(7 downto 0);
           data_out : out std_logic_vector(15 downto 0) );
end component;

component mux_2to1_4b is
    port ( mux_select : in  std_logic;
           data_a     : in  std_logic_vector(3 downto 0);
           data_b     : in  std_logic_vector(3 downto 0);
           data_out   : out std_logic_vector(3 downto 0) );
end component;

component mux_2to1_8b is
    port ( mux_select : in  std_logic;
           data_a     : in  std_logic_vector(7 downto 0);
           data_b     : in  std_logic_vector(7 downto 0);
           data_out   : out std_logic_vector(7 downto 0) );
end component;

component mux_2to1_16b is
    port ( mux_select : in  std_logic;
           data_a     : in  std_logic_vector(15 downto 0);
           data_b     : in  std_logic_vector(15 downto 0);
           data_out   : out std_logic_vector(15 downto 0) );
end component;

component control_unit is
    port ( opcode          : in  std_logic_vector(3 downto 0);
           ext_src         : out std_logic;
           reg_src         : out std_logic;
           reg_dst         : out std_logic;
           reg_write       : out std_logic;
           alu_src         : out std_logic;
			  alu_ctl         : out std_logic_vector(0 to 2);
           mem_write       : out std_logic;
           tag_write       : out std_logic;
           mem_to_reg      : out std_logic;
           tag_to_reg      : out std_logic;
			  pc_to_pc_jump   : out std_logic;
			  pc_to_pc_branch : out std_logic;
			  is_bez          : out std_logic);
end component;

component register_file is
    port ( reset           : in  std_logic;
           clk             : in  std_logic;
           read_register_a : in  std_logic_vector(3 downto 0);
           read_register_b : in  std_logic_vector(3 downto 0);
           write_enable    : in  std_logic;
           write_register  : in  std_logic_vector(3 downto 0);
           write_data      : in  std_logic_vector(15 downto 0);
           read_data_a     : out std_logic_vector(15 downto 0);
           read_data_b     : out std_logic_vector(15 downto 0) );
end component;

component adder_8b is
    port ( src_a     : in  std_logic_vector(7 downto 0);
           src_b     : in  std_logic_vector(7 downto 0);
           sum       : out std_logic_vector(7 downto 0);
           carry_out : out std_logic );
end component;

component adder_16b is
    port ( src_a     : in  std_logic_vector(15 downto 0);
           src_b     : in  std_logic_vector(15 downto 0);
           sum       : out std_logic_vector(15 downto 0);
           carry_out : out std_logic );
end component;

component alu_16b is
    port ( src_a     : in  std_logic_vector(15 downto 0);
           src_b     : in  std_logic_vector(15 downto 0);
			  control   : in  std_logic_vector(0 to 2);
           result    : out std_logic_vector(15 downto 0);
			  zero      : out std_logic;
           carry_out : out std_logic );
end component;

component data_memory is
    port ( reset        : in  std_logic;
           clk          : in  std_logic;
           write_enable : in  std_logic;
           write_data   : in  std_logic_vector(15 downto 0);
           addr_in      : in  std_logic_vector(8 downto 0);
           data_out     : out std_logic_vector(15 downto 0) );
end component;

component tag_memory is
    port ( reset        : in  std_logic;
           clk          : in  std_logic;
           write_enable : in  std_logic;
           write_data   : in  std_logic_vector(15 downto 0);
           addr_in      : in  std_logic_vector(7 downto 0);
           data_out     : out std_logic_vector(15 downto 0) );
end component;

signal sig_next_pc              : std_logic_vector(7 downto 0);
signal sig_curr_pc              : std_logic_vector(7 downto 0);
signal sig_one_8b               : std_logic_vector(7 downto 0);
signal sig_pc_carry_out         : std_logic;
signal sig_insn                 : std_logic_vector(15 downto 0);
signal sig_sign_ext_offset_0    : std_logic_vector(15 downto 0);
signal sig_sign_ext_offset_1    : std_logic_vector(15 downto 0);
signal sig_sign_extended_offset : std_logic_vector(15 downto 0);
signal sig_ext_src              : std_logic;
signal sig_reg_src              : std_logic;
signal sig_reg_dst              : std_logic;
signal sig_reg_write            : std_logic;
signal sig_alu_src              : std_logic;
signal sig_mem_write            : std_logic;
signal sig_tag_write            : std_logic;
signal sig_mem_to_reg           : std_logic;
signal sig_tag_to_reg           : std_logic;
signal sig_write_register       : std_logic_vector(3 downto 0);
signal sig_data_out             : std_logic_vector(15 downto 0);
signal sig_tag_out              : std_logic_vector(15 downto 0);
signal sig_write_data           : std_logic_vector(15 downto 0);
signal sig_read_data_a          : std_logic_vector(15 downto 0);
signal sig_read_data_b          : std_logic_vector(15 downto 0);
signal sig_read_register_b      : std_logic_vector(3 downto 0);
signal sig_alu_src_b            : std_logic_vector(15 downto 0);
signal sig_alu_result           : std_logic_vector(15 downto 0); 
signal sig_alu_carry_out        : std_logic;
signal sig_alu_ctl              : std_logic_vector(0 to 2);
signal sig_data_mem_out         : std_logic_vector(15 downto 0);
signal sig_jump_condition       : std_logic;
signal sig_pc_to_pc_branch      : std_logic;
signal sig_new_pc_branch          : std_logic_vector(7 downto 0);
signal sig_new_pc               : std_logic_vector(7 downto 0);
signal sig_is_bez               : std_logic;
signal sig_zero                 : std_logic;
signal sig_branch_condition     : std_logic;

begin

    sig_one_8b <= "00000001";
    sig_branch_condition <= (sig_pc_to_pc_branch AND sig_zero AND sig_is_bez) OR (sig_pc_to_pc_branch AND NOT sig_zero AND NOT sig_is_bez);
    
	 pc : program_counter
    port map ( reset    => reset,
               clk      => clk,
               addr_in  => sig_new_pc,
               addr_out => sig_curr_pc ); 

    next_pc : adder_8b 
    port map ( src_a     => sig_curr_pc, 
               src_b     => sig_one_8b,
               sum       => sig_next_pc,   
               carry_out => sig_pc_carry_out );
    
    insn_mem : instruction_memory 
    port map ( reset    => reset,
               addr_in  => sig_curr_pc,
               insn_out => sig_insn );

    sign_extend : sign_extend_4to16 
    port map ( data_in  => sig_insn(3 downto 0),
               data_out => sig_sign_ext_offset_0 );
    
    sign_extend_immi : sign_extend_8to16
    port map ( data_in => sig_insn(7 downto 0),
               data_out => sig_sign_ext_offset_1 );
               
    mux_ext_src : mux_2to1_16b
    port map ( mux_select => sig_ext_src,
               data_a     => sig_sign_ext_offset_0,
               data_b     => sig_sign_ext_offset_1,
               data_out   => sig_sign_extended_offset );
               
    ctrl_unit : control_unit 
    port map ( opcode          => sig_insn(15 downto 12),
               ext_src         => sig_ext_src,
               reg_src         => sig_reg_src,
               reg_dst         => sig_reg_dst,
               reg_write       => sig_reg_write,
               alu_src         => sig_alu_src,
					alu_ctl         => sig_alu_ctl,
               mem_write       => sig_mem_write,
               mem_to_reg      => sig_mem_to_reg,
               tag_write       => sig_tag_write,
               tag_to_reg      => sig_tag_to_reg,
					pc_to_pc_jump   => sig_jump_condition ,
					pc_to_pc_branch => sig_pc_to_pc_branch,
					is_bez          => sig_is_bez);
   
    mux_reg_src : mux_2to1_4b 
    port map ( mux_select => sig_reg_src,
               data_a     => sig_insn(11 downto 8),
               data_b     => sig_insn(7 downto 4),
               data_out   => sig_read_register_b );
               
    mux_reg_dst : mux_2to1_4b 
    port map ( mux_select => sig_reg_dst,
               data_a     => sig_read_register_b,
               data_b     => sig_insn(3 downto 0),
               data_out   => sig_write_register );

    reg_file : register_file 
    port map ( reset           => reset, 
               clk             => clk,
               read_register_a => sig_insn(11 downto 8),
               read_register_b => sig_read_register_b,
               write_enable    => sig_reg_write,
               write_register  => sig_write_register,
               write_data      => sig_write_data,
               read_data_a     => sig_read_data_a,
               read_data_b     => sig_read_data_b );
    
    mux_alu_src : mux_2to1_16b 
    port map ( mux_select => sig_alu_src,
               data_a     => sig_read_data_b,
               data_b     => sig_sign_extended_offset,
               data_out   => sig_alu_src_b );

    alu : alu_16b 
    port map ( src_a     => sig_read_data_a,
               src_b     => sig_alu_src_b,
					control   => sig_alu_ctl,
               result    => sig_alu_result,
               carry_out => sig_alu_carry_out,
					zero      => sig_zero);

    data_mem : data_memory 
    port map ( reset        => reset,
               clk          => clk,
               write_enable => sig_mem_write,
               write_data   => sig_read_data_b,
               addr_in      => sig_alu_result(8 downto 0),
               data_out     => sig_data_mem_out );
    
    tag_mem : tag_memory
    port map ( reset        => reset,
               clk          => clk,
               write_enable => sig_tag_write,
               write_data   => sig_read_data_b,
               addr_in      => sig_alu_result(7 downto 0),
               data_out     => sig_tag_out);
               
    mux_data_to_mem : mux_2to1_16b 
    port map ( mux_select => sig_mem_to_reg,
               data_a     => sig_alu_result,
               data_b     => sig_data_mem_out,
               data_out   => sig_data_out );
               
    mux_mem_to_reg : mux_2to1_16b 
    port map ( mux_select => sig_tag_to_reg,
               data_a     => sig_data_out,
               data_b     => sig_tag_out,
               data_out   => sig_write_data );
	 				
	 mux_pc_to_pc_jump : mux_2to1_8b
	 port map ( mux_select => sig_jump_condition ,
               data_a     => sig_new_pc_branch,
					data_b     => sig_alu_result(7 downto 0),
					data_out   => sig_new_pc);
					
	 mux_pc_to_pc_branch : mux_2to1_8b
	 port map ( mux_select => sig_branch_condition,
               data_a     => sig_next_pc,
					data_b     => sig_insn(7 downto 0),
					data_out   => sig_new_pc_branch);
			
end structural;
