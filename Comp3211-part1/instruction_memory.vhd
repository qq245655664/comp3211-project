---------------------------------------------------------------------------
-- instruction_memory.vhd - Implementation of A Single-Port, 16 x 16-bit
--                          Instruction Memory.
-- 
-- Notes: refer to headers in single_cycle_core.vhd for the supported ISA.
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_TEXTIO.ALL;
use STD.TEXTIO.ALL;

entity instruction_memory is
    port ( reset    : in  std_logic;
           addr_in  : in  std_logic_vector(7 downto 0);
           insn_out : out std_logic_vector(15 downto 0) );
end instruction_memory;

architecture behavioral of instruction_memory is

type mem_array is array(0 to 255) of std_logic_vector(15 downto 0);
signal sig_insn_mem : mem_array := ((others => (others => '0')));
file insn_file : text open read_mode is "instructions.txt";

begin


    mem_process: process ( addr_in ) is
 
    variable var_addr     : integer;
	 variable line_buf     : line;
	 variable data_buf     : std_logic_vector(15 downto 0);
    begin
        if (reset = '1') then
        for i in 0 to 255 loop
		     if (not endfile(insn_file)) then
					readline(insn_file,line_buf);
					read(line_buf,data_buf);
					sig_insn_mem(i) <= data_buf;
		     end if;
		  end loop;
		  else
            -- read instructions on the rising clock edge
            var_addr := conv_integer(addr_in);
            insn_out <= sig_insn_mem(var_addr);
        end if;

        -- the following are probe signals (for simulation purpose)
        --sig_insn_mem <= var_insn_mem;

    end process;
  
end behavioral;
