library ieee;
use ieee.std_logic_1164.all;

entity mem_wb_register is
   port ( reset                  : in std_logic;
          clk                    : in std_logic;
          tag_to_reg_mem         : in std_logic;
          mem_to_reg_mem         : in std_logic;
		    reg_write_mem			   : in std_logic;
		    alu_result_mem         : in std_logic_vector(15 downto 0);
          data_mem_out_mem       : in std_logic_vector(15 downto 0);
          data_tag_out_mem       : in std_logic_vector(15 downto 0);
          write_reg_mem          : in std_logic_vector (3 downto 0);
          op_code_mem            : in std_logic_vector (3 downto 0);
          read_reg_a_mem         : in std_logic_vector (3 downto 0);
          read_reg_b_mem         : in std_logic_vector (3 downto 0);
          tag_to_reg_wb          : out std_logic;
          mem_to_reg_wb          : out std_logic;
          reg_write_wb           : out std_logic;
		    alu_result_wb          : out std_logic_vector(15 downto 0);
          data_mem_out_wb        : out std_logic_vector(15 downto 0);
          data_tag_out_wb        : out std_logic_vector(15 downto 0);
          write_reg_wb           : out std_logic_vector(3 downto 0);
          op_code_wb             : out std_logic_vector(3 downto 0);
          read_reg_a_wb          : out std_logic_vector(3 downto 0);
          read_reg_b_wb          : out std_logic_vector(3 downto 0));
end mem_wb_register;

architecture behaviour of mem_wb_register is
begin
   process(reset, clk) is
	begin
      if (reset = '1') then
         tag_to_reg_wb <= '0';
         mem_to_reg_wb <= '0';
		 reg_write_wb <= '0';
		 alu_result_wb <= (others => '0');
         data_mem_out_wb <= (others => '0');
         data_tag_out_wb <= (others => '0');
         write_reg_wb <= (others => '0');
         op_code_wb <= (others => '0');
         read_reg_a_wb <= (others => '0');
         read_reg_b_wb <= (others => '0');
      elsif (falling_edge(clk)) then
         tag_to_reg_wb <= tag_to_reg_mem after 0.5 ns;
         mem_to_reg_wb <= mem_to_reg_mem after 0.5 ns;
		 reg_write_wb <= reg_write_mem after 0.5 ns;
		 alu_result_wb <= alu_result_mem after 0.5 ns;
         data_mem_out_wb <= data_mem_out_mem after 0.5 ns;
         data_tag_out_wb <= data_tag_out_mem after 0.5 ns;
         write_reg_wb <= write_reg_mem after 0.5 ns;
         op_code_wb <= op_code_mem after 0.5 ns;
         read_reg_a_wb <= read_reg_a_mem after 0.5 ns;
         read_reg_b_wb <= read_reg_b_mem after 0.5 ns;
      end if;
   end process;
end behaviour;