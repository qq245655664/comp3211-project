----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:40:07 04/27/2016 
-- Design Name: 
-- Module Name:    stall_units - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity stall_unit is
    Port (op_code_mem     :  in std_logic_vector(3 downto 0);
		    branch_condition:  in std_logic;
		    op_code_ex      :  in std_logic_vector(3 downto 0);
		    op_code_id      :  in std_logic_vector(3 downto 0);
		    rt_id           :  in std_logic_vector(3 downto 0);
		    rs_id           :  in std_logic_vector(3 downto 0);
		    rt_ex           :  in std_logic_vector(3 downto 0);
		    flush           :  out std_logic;
		    stall           :  out std_logic);
end stall_unit;

architecture Behavioral of stall_unit is
constant OP_LOAD  : std_logic_vector(3 downto 0) := "0010";
constant OP_LOADT : std_logic_vector(3 downto 0) := "0101";
constant OP_BEZ    : std_logic_vector(3 downto 0) := "1110";
constant OP_BNZ   : std_logic_vector(3 downto 0) := "1111";
constant OP_ADDI  : std_logic_vector(3 downto 0) := "0100";
constant OP_JUMP  : std_logic_vector(3 downto 0) := "1100";


begin
	stall <= '1'   after 1.5 ns when (op_code_ex = OP_LOAD or op_code_ex = OP_LOADT) 
						             and (op_code_id = OP_ADDI or op_code_id = OP_JUMP)
						             and (rs_id = rt_ex) else
				'1'   after 1.5 ns when	(op_code_ex = OP_LOAD or op_code_ex = OP_LOADT) 
			                      and not (op_code_id = OP_ADDI or op_code_id = OP_JUMP) 
						             and (rt_id = rt_ex or rs_id = rt_ex) else
				'0'   after 1.5 ns;
				
	flush <= '1'   after 1.5 ns when (op_code_mem = OP_JUMP) 
	               or ((op_code_mem = OP_BEZ or op_code_mem = OP_BNZ) and (branch_condition = '1')) else
				'0'   after 1.5 ns;						 
end Behavioral;



