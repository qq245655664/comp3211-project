---------------------------------------------------------------------------
---------------------------------------------------------------------------
-- single_cycle_core.vhd - A Single-Cycle Processor Implementation
--
-- Notes : 
--
-- See single_cycle_core.pdf for the block diagram of this single
-- cycle processor core.
--
-- Instruction Set Architecture (ISA) for the single-cycle-core:
--   Each instruction is 16-bit wide, with four 4-bit fields.

-- noop      
--    # no operation or to signal end of program
--    # format:  | opcode = 0 |  0   |  0   |   0    |

-- slt   rd, rs, rt
--    # rd <- (rs < rt) ? 1 : 0
--    # format:  | opcode = 1 |  rs  |  rt  |   rd   |

-- load  rt, rs, offset     
--    # load data at memory location (rs + offset) into rt
--    # format:  | opcode = 2 |  rs  |  rt  | offset |

-- store rt, rs, offset
--    # store data rt into memory location (rs + offset)
--    # format:  | opcode = 3 |  rs  |  rt  | offset |

-- addi  rs, $immediate
--    # rs <- rs + $immediate
--    # format:  | opcode = 4 |  rs  |  $immediate   |

-- loadt rt, rs, offset     
--    # load tag at tagmem location (rs + offset) into rt
--    # format:  | opcode = 5 |  rs  |  rt  | offset |

-- sll   rt, rs, shamt
--    # rt <- rs << Shamt
--    # format:  | opcode = 6 |  rs  |  rt  | shamt |

-- srl   rt, rs, shamt
--    # rt <- rs >> Shamt
--    # format:  | opcode = 7 |  rs  |  rt  | shamt |

-- add   rd, rs, rt
--    # rd <- rs + rt
--    # format:  | opcode = 8 |  rs  |  rt  |   rd   |

-- sub   rd, rs, rt
--    # rd <- rs - rt
--    # format:  | opcode = 9 |  rs  |  rt  |   rd   | 
--        
-- and   rd, rs, rt
--    # rd <- rs & rt
--    # format:  | opcode = A |  rs  |  rt  |   rd   |

-- or    rd, rs, rt
--    # rd <- rs | rt
--    # format:  | opcode = B |  rs  |  rt  |   rd   |

-- jump  rs, offset
--    # PC = rs + offset
--    # format:  | opcode = C |  rs  | 0000 | offset |

-- storet rt, rs, offset
--    # store tag rt into tag_memory location (rs + offset)
--    # format:  | opcode = D |  rs  |  rt  | offset |

-- bez   rt, $branchAddr
--    # if (rs == 0) { PC = PC + 1 + $branchAddr }
--    # format:  | opcode = E |  rt  |  $branchAddr  |

-- bnz   rt, $branchAddr
--    # if (rs != 0) { PC = PC + 1 + $branchAddr }
--    # format:  | opcode = F |  rt  |  $branchAddr  |

-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity single_cycle_core is
    port ( reset  : in  std_logic;
           clk    : in  std_logic;
           done   : out std_logic);
end single_cycle_core;

architecture structural of single_cycle_core is

component program_counter is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(7 downto 0);
           addr_out : out std_logic_vector(7 downto 0);
			  stall    : in  std_logic);
end component;

component instruction_memory is
    port ( reset    : in  std_logic;
           addr_in  : in  std_logic_vector(7 downto 0);
           insn_out : out std_logic_vector(15 downto 0) );
end component;

component sign_extend_4to16 is
    port ( data_in  : in  std_logic_vector(3 downto 0);
           data_out : out std_logic_vector(15 downto 0) );
end component;

component sign_extend_8to16 is
    port ( data_in  : in  std_logic_vector(7 downto 0);
           data_out : out std_logic_vector(15 downto 0) );
end component;

component mux_2to1_4b is
    port ( mux_select : in  std_logic;
           data_a     : in  std_logic_vector(3 downto 0);
           data_b     : in  std_logic_vector(3 downto 0);
           data_out   : out std_logic_vector(3 downto 0) );
end component;

component mux_2to1_8b is
    port ( mux_select : in  std_logic;
           data_a     : in  std_logic_vector(7 downto 0);
           data_b     : in  std_logic_vector(7 downto 0);
           data_out   : out std_logic_vector(7 downto 0) );
end component;

component mux_2to1_16b is
    port ( mux_select : in  std_logic;
           data_a     : in  std_logic_vector(15 downto 0);
           data_b     : in  std_logic_vector(15 downto 0);
           data_out   : out std_logic_vector(15 downto 0) );
end component;

component control_unit is
    port ( opcode          : in  std_logic_vector(3 downto 0);
           ext_src         : out std_logic;
           reg_src         : out std_logic;
           reg_dst         : out std_logic;
           reg_write       : out std_logic;
           alu_src         : out std_logic;
			  alu_ctl         : out std_logic_vector(0 to 2);
           mem_write       : out std_logic;
           tag_write       : out std_logic;
           mem_to_reg      : out std_logic;
           tag_to_reg      : out std_logic;
			  pc_to_pc_jump   : out std_logic;
			  pc_to_pc_branch : out std_logic;
			  is_bez          : out std_logic);
end component;

component register_file is
    port ( reset           : in  std_logic;
           clk             : in  std_logic;
           read_register_a : in  std_logic_vector(3 downto 0);
           read_register_b : in  std_logic_vector(3 downto 0);
           write_enable    : in  std_logic;
           write_register  : in  std_logic_vector(3 downto 0);
           write_data      : in  std_logic_vector(15 downto 0);
           read_data_a     : out std_logic_vector(15 downto 0);
           read_data_b     : out std_logic_vector(15 downto 0) );
end component;

component adder_8b is
    port ( src_a     : in  std_logic_vector(7 downto 0);
           src_b     : in  std_logic_vector(7 downto 0);
           sum       : out std_logic_vector(7 downto 0);
           carry_out : out std_logic );
end component;

component adder_16b is
    port ( src_a     : in  std_logic_vector(15 downto 0);
           src_b     : in  std_logic_vector(15 downto 0);
           sum       : out std_logic_vector(15 downto 0);
           carry_out : out std_logic );
end component;

component alu_16b is
    port ( src_a     : in  std_logic_vector(15 downto 0);
           src_b     : in  std_logic_vector(15 downto 0);
			  control   : in  std_logic_vector(0 to 2);
           result    : out std_logic_vector(15 downto 0);
			  zero      : out std_logic;
           carry_out : out std_logic );
end component;

component data_memory is
    port ( reset        : in  std_logic;
           clk          : in  std_logic;
           write_enable : in  std_logic;
           write_data   : in  std_logic_vector(15 downto 0);
           addr_in      : in  std_logic_vector(8 downto 0);
           data_out     : out std_logic_vector(15 downto 0);
           test_finished: out std_logic);
end component;

component tag_memory is
    port ( reset        : in  std_logic;
           clk          : in  std_logic;
           write_enable : in  std_logic;
           write_data   : in  std_logic_vector(15 downto 0);
           addr_in      : in  std_logic_vector(7 downto 0);
           data_out     : out std_logic_vector(15 downto 0) );
end component;

--Forwarding Unit
component forwarding_unit is
    port ( mem_wb_reg_write : in std_logic;
           mem_wb_reg_rd    : in std_logic_vector(3 downto 0);
           mem_wb_reg_rt    : in std_logic_vector(3 downto 0);
           mem_wb_reg_rs    : in std_logic_vector(3 downto 0);
           ex_mem_reg_write : in std_logic;
           ex_mem_reg_rd    : in std_logic_vector(3 downto 0);
           ex_mem_reg_rt    : in std_logic_vector(3 downto 0);
           ex_mem_reg_rs    : in std_logic_vector(3 downto 0);
           id_ex_reg_rs     : in std_logic_vector(3 downto 0);
           id_ex_reg_rt     : in std_logic_vector(3 downto 0);
           ex_mem_opcode    : in std_logic_vector(3 downto 0);
           mem_wb_opcode    : in std_logic_vector(3 downto 0);
           id_ex_opcode     : in std_logic_vector(3 downto 0);
           sel_alu_src_a    : out std_logic_vector(0 to 1); -- 00:origin 01:mem_result 10:write_data
           sel_alu_src_b    : out std_logic_vector(0 to 1);
			  sel_store_val    : out std_logic_vector(0 to 1); -- choose src of write address
			  use_extend       : in std_logic);
end component;

component mux_4to1_16b is
    port (mux_select : in std_logic_vector(0 to 1);
          data_a     : in std_logic_vector(15 downto 0);
          data_b     : in std_logic_vector(15 downto 0);
          data_c     : in std_logic_vector(15 downto 0);
          data_d     : in std_logic_vector(15 downto 0);
          data_out   : out std_logic_vector(15 downto 0));
end component;

--Pipeline registers
component if_id_register is
   port( reset      : in std_logic;
         clk        : in std_logic;
         insn_if    : in std_logic_vector(15 downto 0);
			flush      : in std_logic;
		   stall      : in std_logic;
         insn_id    : out std_logic_vector(15 downto 0));
end component;

component id_ex_register is
   port(reset                  	: in std_logic;
		clk                     : in std_logic;
		alu_src_id              : in std_logic;
		alu_ctl_id              : in std_logic_vector(2 downto 0);
		tag_write_id            : in std_logic;
		mem_write_id            : in std_logic;
		tag_to_reg_id           : in std_logic;
		mem_to_reg_id           : in std_logic;
		jump_condition_id		: in std_logic;
		pc_to_pc_branch_id      : in std_logic;
		is_bez_id               : in std_logic;
		reg_write_id			: in std_logic;
		read_data_a_id          : in std_logic_vector(15 downto 0);
		read_data_b_id          : in std_logic_vector(15 downto 0);
		sign_extended_offset_id : in std_logic_vector(15 downto 0);
		read_reg_a_id			: in std_logic_vector(3 downto 0);
		read_reg_b_id			: in std_logic_vector(3 downto 0);
		write_reg_id			: in std_logic_vector(3 downto 0);
		op_code_id				: in std_logic_vector(3 downto 0);
		branch_addr_id			: in std_logic_vector(7 downto 0);
		flush                : in std_logic;
		alu_src_ex              : out std_logic;
		alu_ctl_ex              : out std_logic_vector(2 downto 0);
		tag_write_ex            : out std_logic;
		mem_write_ex            : out std_logic;
		tag_to_reg_ex           : out std_logic;
		mem_to_reg_ex           : out std_logic;
		jump_condition_ex		: out std_logic;
		pc_to_pc_branch_ex      : out std_logic;
		is_bez_ex               : out std_logic;
		reg_write_ex			: out std_logic;
		read_data_a_ex          : out std_logic_vector(15 downto 0);
		read_data_b_ex          : out std_logic_vector(15 downto 0);
		sign_extended_offset_ex : out std_logic_vector(15 downto 0);
		read_reg_a_ex			: out std_logic_vector(3 downto 0);
		read_reg_b_ex			: out std_logic_vector(3 downto 0);
		write_reg_ex			: out std_logic_vector(3 downto 0);
		op_code_ex				: out std_logic_vector(3 downto 0);
		branch_addr_ex			: out std_logic_vector(7 downto 0));
end component;

component ex_mem_register is
   port(reset              : in std_logic;
       clk                 : in std_logic;
       tag_write_ex        : in std_logic;
       mem_write_ex        : in std_logic;
       tag_to_reg_ex       : in std_logic;
       mem_to_reg_ex       : in std_logic;
       jump_condition_ex   : in std_logic;
       pc_to_pc_branch_ex  : in std_logic;
       is_bez_ex           : in std_logic;
	    reg_write_ex		   : in std_logic;
	    zero_ex			   : in std_logic;
	    alu_result_ex       : in std_logic_vector(15 downto 0);
       read_data_b_ex      : in std_logic_vector(15 downto 0);
       write_reg_ex        : in std_logic_vector(3 downto 0);
	    branch_addr_ex	   : in std_logic_vector(7 downto 0);
       op_code_ex       : in std_logic_vector(3 downto 0);
       read_reg_a_ex       : in std_logic_vector(3 downto 0);
       read_reg_b_ex       : in std_logic_vector(3 downto 0);
		 flush               : in std_logic;
       tag_write_mem       : out std_logic;
       mem_write_mem       : out std_logic;
       tag_to_reg_mem      : out std_logic;
       mem_to_reg_mem      : out std_logic;
       jump_condition_mem  : out std_logic;
       pc_to_pc_branch_mem : out std_logic;
       is_bez_mem          : out std_logic;
	    reg_write_mem	   : out std_logic;
	    zero_mem			   : out std_logic;
	    alu_result_mem      : out std_logic_vector(15 downto 0);
       read_data_b_mem     : out std_logic_vector(15 downto 0);
       write_reg_mem       : out std_logic_vector(3 downto 0);
	    branch_addr_mem	   : out std_logic_vector(7 downto 0);
       op_code_mem       : out std_logic_vector(3 downto 0);
       read_reg_a_mem       : out std_logic_vector(3 downto 0);
       read_reg_b_mem       : out std_logic_vector(3 downto 0));
end component;

component mem_wb_register is
   port ( reset                  : in std_logic;
          clk                    : in std_logic;
          tag_to_reg_mem         : in std_logic;
          mem_to_reg_mem         : in std_logic;
		    reg_write_mem			   : in std_logic;
		    alu_result_mem         : in std_logic_vector(15 downto 0);
          data_mem_out_mem       : in std_logic_vector(15 downto 0);
          data_tag_out_mem       : in std_logic_vector(15 downto 0);
          write_reg_mem          : in std_logic_vector (3 downto 0);
          op_code_mem            : in std_logic_vector (3 downto 0);
          read_reg_a_mem         : in std_logic_vector (3 downto 0);
          read_reg_b_mem         : in std_logic_vector (3 downto 0);
          tag_to_reg_wb          : out std_logic;
          mem_to_reg_wb          : out std_logic;
          reg_write_wb           : out std_logic;
		    alu_result_wb          : out std_logic_vector(15 downto 0);
          data_mem_out_wb        : out std_logic_vector(15 downto 0);
          data_tag_out_wb        : out std_logic_vector(15 downto 0);
          write_reg_wb           : out std_logic_vector(3 downto 0);
          op_code_wb             : out std_logic_vector(3 downto 0);
          read_reg_a_wb          : out std_logic_vector(3 downto 0);
          read_reg_b_wb          : out std_logic_vector(3 downto 0));
end component;

component stall_unit is
    Port (op_code_mem     :  in std_logic_vector(3 downto 0);
		    branch_condition:  in std_logic;
		    op_code_ex      :  in std_logic_vector(3 downto 0);
		    op_code_id      :  in std_logic_vector(3 downto 0);
		    rt_id           :  in std_logic_vector(3 downto 0);
		    rs_id           :  in std_logic_vector(3 downto 0);
		    rt_ex           :  in std_logic_vector(3 downto 0);
		    flush           :  out std_logic;
		    stall           :  out std_logic);
end component;


--Forwarding Unit Signal
signal sig_alu_src_a_final      : std_logic_vector(15 downto 0);
signal sig_alu_src_b_final      : std_logic_vector(15 downto 0);
signal sig_alu_src_a_sel        : std_logic_vector(0 to 1);
signal sig_alu_src_b_sel        : std_logic_vector(0 to 1);
signal sig_store_val_sel        : std_logic_vector(0 to 1);

--Stall Unit related signals
signal sig_stall                : std_logic;
signal sig_flush                : std_logic;

--Single Cycle Core Signals (some are multi staged)
signal sig_next_pc              : std_logic_vector(7 downto 0);
signal sig_curr_pc              : std_logic_vector(7 downto 0);
signal sig_one_8b               : std_logic_vector(7 downto 0);
signal sig_pc_carry_out         : std_logic; --unused

signal sig_insn_if              : std_logic_vector(15 downto 0);
signal sig_insn_id				  : std_logic_vector(15 downto 0);

signal sig_sign_ext_offset_0    		: std_logic_vector(15 downto 0);
signal sig_sign_ext_offset_1			: std_logic_vector(15 downto 0);
signal sig_sign_extended_offset_id	: std_logic_vector(15 downto 0);
signal sig_sign_extended_offset_ex	: std_logic_vector(15 downto 0);

signal sig_alu_src_b       : std_logic_vector(15 downto 0);
signal sig_new_pc_branch	: std_logic_vector(7 downto 0);
signal sig_new_pc				: std_logic_vector(7 downto 0);

signal sig_zero_ex				: std_logic;
signal sig_zero_mem				: std_logic;

signal sig_branch_condition	: std_logic;
signal sig_data_out        : std_logic_vector(15 downto 0);
signal sig_write_data		: std_logic_vector(15 downto 0);
signal sig_alu_carry_out   : std_logic; --unused

signal sig_if_id_flush        : std_logic;
signal sig_id_ex_flush        : std_logic;     
signal sig_ex_mem_flush       : std_logic;


--Control signals
--ID stage
signal sig_alu_src_id			: std_logic;
signal sig_alu_ctl_id			: std_logic_vector(0 to 2);
signal sig_ext_src	         : std_logic;
signal sig_reg_src   	      : std_logic;
signal sig_reg_dst				: std_logic;
signal sig_write_reg_id		   : std_logic_vector(3 downto 0);
signal sig_read_reg_a_id	   : std_logic_vector(3 downto 0);
signal sig_read_reg_b_id	   : std_logic_vector(3 downto 0);
signal sig_read_data_a_id     : std_logic_vector(15 downto 0);
signal sig_read_data_b_id     : std_logic_vector(15 downto 0);
signal sig_mem_write_id       : std_logic;
signal sig_tag_write_id       : std_logic;
signal sig_pc_to_pc_branch_id   : std_logic;
signal sig_jump_condition_id    : std_logic;
signal sig_is_bez_id            : std_logic;
signal sig_mem_to_reg_id        : std_logic;
signal sig_tag_to_reg_id        : std_logic;
signal sig_reg_write_id         : std_logic;
signal sig_op_code_id           : std_logic_vector(3 downto 0);
signal sig_branch_addr_id       : std_logic_vector(7 downto 0);

--EX stage
signal sig_alu_src_ex			   : std_logic;
signal sig_alu_ctl_ex			   : std_logic_vector(0 to 2);
signal sig_write_reg_ex		      : std_logic_vector(3 downto 0);
signal sig_read_reg_a_ex	      : std_logic_vector(3 downto 0);
signal sig_read_reg_b_ex         : std_logic_vector(3 downto 0);
signal sig_read_data_a_ex        : std_logic_vector(15 downto 0);
signal sig_read_data_b_ex        : std_logic_vector(15 downto 0);
signal sig_mem_write_ex          : std_logic;
signal sig_tag_write_ex          : std_logic;
signal sig_pc_to_pc_branch_ex    : std_logic;
signal sig_jump_condition_ex     : std_logic;
signal sig_is_bez_ex             : std_logic;
signal sig_mem_to_reg_ex         : std_logic;
signal sig_tag_to_reg_ex         : std_logic;
signal sig_reg_write_ex          : std_logic;
signal sig_alu_result_ex	      : std_logic_vector(15 downto 0);
signal sig_op_code_ex            : std_logic_vector(3 downto 0);
signal sig_branch_addr_ex        : std_logic_vector(7 downto 0);
signal sig_store_val_ex          : std_logic_vector(15 downto 0);

--MEM stage
signal sig_mem_write_mem        : std_logic;
signal sig_tag_write_mem        : std_logic;
signal sig_pc_to_pc_branch_mem  : std_logic;
signal sig_jump_condition_mem   : std_logic;
signal sig_is_bez_mem           : std_logic;
signal sig_read_reg_a_mem       : std_logic_vector(3 downto 0);
signal sig_read_reg_b_mem       : std_logic_vector(3 downto 0);
signal sig_write_reg_mem	     : std_logic_vector(3 downto 0);
signal sig_read_data_b_mem	     : std_logic_vector(15 downto 0);
signal sig_data_mem_out_mem     : std_logic_vector(15 downto 0);
signal sig_data_tag_out_mem     : std_logic_vector(15 downto 0);
signal sig_mem_to_reg_mem       : std_logic;
signal sig_tag_to_reg_mem       : std_logic;
signal sig_reg_write_mem        : std_logic;
signal sig_alu_result_mem	     : std_logic_vector(15 downto 0);
signal sig_op_code_mem          : std_logic_vector(3 downto 0);
signal sig_branch_addr_mem      : std_logic_vector(7 downto 0);

--WB stage
signal sig_mem_to_reg_wb        : std_logic;
signal sig_tag_to_reg_wb	     : std_logic;
signal sig_reg_write_wb         : std_logic;
signal sig_read_reg_a_wb        : std_logic_vector(3 downto 0);
signal sig_read_reg_b_wb        : std_logic_vector(3 downto 0);
signal sig_write_reg_wb		     : std_logic_vector(3 downto 0);
signal sig_data_mem_out_wb	     : std_logic_vector(15 downto 0);
signal sig_data_tag_out_wb	     : std_logic_vector(15 downto 0);
signal sig_alu_result_wb	     : std_logic_vector(15 downto 0);
signal sig_op_code_wb           : std_logic_vector(3 downto 0);

begin
    sig_one_8b <= "00000001";
    sig_branch_condition <= (sig_pc_to_pc_branch_mem AND sig_zero_mem AND sig_is_bez_mem) OR (sig_pc_to_pc_branch_mem AND NOT sig_zero_mem AND NOT sig_is_bez_mem);
    
	 sig_op_code_id <= sig_insn_id(15 downto 12);
	 sig_read_reg_a_id <= sig_insn_id(11 downto 8);
    sig_branch_addr_id <= sig_insn_id(7 downto 0);
	 
	 sig_if_id_flush <= sig_flush;
	 sig_id_ex_flush <= sig_flush or sig_stall;
	 sig_ex_mem_flush <= sig_flush;
	 
	 pc : program_counter
    port map ( reset    => reset,
               clk      => clk,
               addr_in  => sig_new_pc,
					stall    => sig_stall,
               addr_out => sig_curr_pc ); 

    next_pc : adder_8b 
    port map ( src_a     => sig_curr_pc,
               src_b     => sig_one_8b,
               sum       => sig_next_pc,  
               carry_out => sig_pc_carry_out );
    
    insn_mem : instruction_memory 
    port map ( reset    => reset,
               addr_in  => sig_curr_pc,
               insn_out => sig_insn_if );

	 if_id_reg: if_id_register
	 port map ( reset => reset,
					clk => clk,
					flush => sig_if_id_flush,
					stall => sig_stall,
					insn_if => sig_insn_if,
					insn_id => sig_insn_id );

    sign_extend : sign_extend_4to16 
    port map ( data_in  => sig_insn_id(3 downto 0),
               data_out => sig_sign_ext_offset_0 );
    
    sign_extend_immi : sign_extend_8to16
    port map ( data_in => sig_insn_id(7 downto 0),
               data_out => sig_sign_ext_offset_1 );
               
    mux_ext_src : mux_2to1_16b
    port map ( mux_select => sig_ext_src,
               data_a     => sig_sign_ext_offset_0,
               data_b     => sig_sign_ext_offset_1,
               data_out   => sig_sign_extended_offset_id );
               
    ctrl_unit : control_unit 
    port map ( opcode          => sig_insn_id(15 downto 12),
               ext_src         => sig_ext_src,
               reg_src         => sig_reg_src,
               reg_dst         => sig_reg_dst,
               reg_write       => sig_reg_write_id,
               alu_src         => sig_alu_src_id,
					alu_ctl         => sig_alu_ctl_id,
               mem_write       => sig_mem_write_id,
               mem_to_reg      => sig_mem_to_reg_id,
               tag_write       => sig_tag_write_id,
               tag_to_reg      => sig_tag_to_reg_id,
					pc_to_pc_jump   => sig_jump_condition_id,
					pc_to_pc_branch => sig_pc_to_pc_branch_id,
					is_bez          => sig_is_bez_id);
   
    mux_reg_src : mux_2to1_4b 
    port map ( mux_select => sig_reg_src,
               data_a     => sig_insn_id(11 downto 8),
               data_b     => sig_insn_id(7 downto 4),
               data_out   => sig_read_reg_b_id );
               
    mux_reg_dst : mux_2to1_4b 
    port map ( mux_select => sig_reg_dst,
               data_a     => sig_read_reg_b_id,
               data_b     => sig_insn_id(3 downto 0),
               data_out   => sig_write_reg_id );

    reg_file : register_file
    port map ( reset           => reset, 
               clk             => clk,
               read_register_a => sig_read_reg_a_id,
               read_register_b => sig_read_reg_b_id,
               write_enable    => sig_reg_write_wb,
               write_register  => sig_write_reg_wb,
               write_data      => sig_write_data,
               read_data_a     => sig_read_data_a_id,
               read_data_b     => sig_read_data_b_id );
    
	 
	 id_ex_reg: id_ex_register
	 port map ( reset => reset,
				   clk => clk,
					flush => sig_id_ex_flush,
				   alu_src_id => sig_alu_src_id,
				   alu_ctl_id => sig_alu_ctl_id,
				   tag_write_id => sig_tag_write_id,
				   mem_write_id => sig_mem_write_id,
				   tag_to_reg_id => sig_tag_to_reg_id,
				   mem_to_reg_id => sig_mem_to_reg_id,
				   jump_condition_id => sig_jump_condition_id,
				   pc_to_pc_branch_id => sig_pc_to_pc_branch_id,
				   is_bez_id => sig_is_bez_id,
				   reg_write_id => sig_reg_write_id,
					read_data_a_id => sig_read_data_a_id,
				   read_data_b_id => sig_read_data_b_id,
					sign_extended_offset_id => sig_sign_extended_offset_id,
 				   read_reg_a_id => sig_read_reg_a_id,
				   read_reg_b_id => sig_read_reg_b_id,
				   write_reg_id => sig_write_reg_id,
					op_code_id => sig_op_code_id,
					branch_addr_id => sig_branch_addr_id,
				   alu_src_ex => sig_alu_src_ex,
				   alu_ctl_ex => sig_alu_ctl_ex,
				   tag_write_ex => sig_tag_write_ex,
				   mem_write_ex => sig_mem_write_ex,
				   tag_to_reg_ex => sig_tag_to_reg_ex,
				   mem_to_reg_ex => sig_mem_to_reg_ex,
				   jump_condition_ex => sig_jump_condition_ex,
				   pc_to_pc_branch_ex => sig_pc_to_pc_branch_ex,
				   is_bez_ex => sig_is_bez_ex,
					reg_write_ex => sig_reg_write_ex,
					read_data_a_ex => sig_read_data_a_ex,
				   read_data_b_ex => sig_read_data_b_ex,
				   sign_extended_offset_ex => sig_sign_extended_offset_ex,
				   read_reg_a_ex => sig_read_reg_a_ex,
				   read_reg_b_ex => sig_read_reg_b_ex,
				   write_reg_ex => sig_write_reg_ex,
					op_code_ex => sig_op_code_ex,
					branch_addr_ex => sig_branch_addr_ex);
	

    alu : alu_16b 
    port map ( src_a     => sig_alu_src_a_final,
               src_b     => sig_alu_src_b_final,
					control   => sig_alu_ctl_ex,
               result    => sig_alu_result_ex,
               carry_out => sig_alu_carry_out,
					zero      => sig_zero_ex);
    
    mux_alu_src_a_sel : mux_4to1_16b
    port map ( mux_select => sig_alu_src_a_sel,
               data_a     => sig_read_data_a_ex,
               data_b     => sig_alu_result_mem,
               data_c     => sig_write_data,
               data_d     => (others => '0'),
               data_out   => sig_alu_src_a_final);
    
    mux_alu_src_b_sel : mux_4to1_16b
    port map ( mux_select => sig_alu_src_b_sel,
               data_a     => sig_read_data_b_ex,------ changed
               data_b     => sig_alu_result_mem,------ changed
               data_c     => sig_write_data,
               data_d     => sig_sign_extended_offset_ex,
               data_out   => sig_alu_src_b_final);
					
	mux_store_val_sel : mux_4to1_16b
    port map ( mux_select => sig_store_val_sel,
               data_a     => sig_read_data_b_ex,------ changed
               data_b     => sig_alu_result_mem,------ changed
               data_c     => sig_write_data,
               data_d     => (others => '0'),
               data_out   => sig_store_val_ex);				
	
               
   fdunit : forwarding_unit
   port map ( mem_wb_reg_write => sig_reg_write_wb,
	        use_extend       => sig_alu_src_ex,
           mem_wb_reg_rd    => sig_write_reg_wb,
           mem_wb_reg_rt    => sig_read_reg_b_wb,
           mem_wb_reg_rs    => sig_read_reg_a_wb,
           ex_mem_reg_write => sig_reg_write_mem,
           ex_mem_reg_rd    => sig_write_reg_mem,
           ex_mem_reg_rt    => sig_read_reg_b_mem,-----
           ex_mem_reg_rs    => sig_read_reg_a_mem,
           id_ex_reg_rs     => sig_read_reg_a_ex,
           id_ex_reg_rt     => sig_read_reg_b_ex,
           ex_mem_opcode    => sig_op_code_mem,
           mem_wb_opcode    => sig_op_code_wb,
           sel_alu_src_a    => sig_alu_src_a_sel,
           sel_alu_src_b    => sig_alu_src_b_sel,
           id_ex_opcode     => sig_op_code_ex,
			  sel_store_val    => sig_store_val_sel);


	 ex_mem_reg: ex_mem_register
	 port map ( reset => reset,
				   clk => clk,
					flush => sig_ex_mem_flush,
				   tag_write_ex => sig_tag_write_ex,
				   mem_write_ex => sig_mem_write_ex,
				   tag_to_reg_ex => sig_tag_to_reg_ex,
				   mem_to_reg_ex => sig_mem_to_reg_ex,
				   jump_condition_ex => sig_jump_condition_ex,
				   pc_to_pc_branch_ex => sig_pc_to_pc_branch_ex,
				   is_bez_ex => sig_is_bez_ex,
				   reg_write_ex => sig_reg_write_ex,
					zero_ex => sig_zero_ex,
					alu_result_ex => sig_alu_result_ex,
				   read_data_b_ex => sig_store_val_ex,---- changed
				   write_reg_ex => sig_write_reg_ex,
					branch_addr_ex => sig_branch_addr_ex,
               op_code_ex => sig_op_code_ex,
               read_reg_a_ex => sig_read_reg_a_ex,
               read_reg_b_ex => sig_read_reg_b_ex,
				   tag_write_mem => sig_tag_write_mem,
				   mem_write_mem => sig_mem_write_mem,
				   tag_to_reg_mem => sig_tag_to_reg_mem,
				   mem_to_reg_mem => sig_mem_to_reg_mem,
				   jump_condition_mem => sig_jump_condition_mem,
				   pc_to_pc_branch_mem => sig_pc_to_pc_branch_mem,
				   is_bez_mem => sig_is_bez_mem,
				   reg_write_mem => sig_reg_write_mem,
					zero_mem => sig_zero_mem,
					alu_result_mem => sig_alu_result_mem,
				   read_data_b_mem => sig_read_data_b_mem,
				   write_reg_mem => sig_write_reg_mem,
					branch_addr_mem => sig_branch_addr_mem,
               op_code_mem => sig_op_code_mem,
               read_reg_a_mem => sig_read_reg_a_mem,
               read_reg_b_mem => sig_read_reg_b_mem);

    data_mem : data_memory 
    port map ( reset        => reset,
               clk          => clk,
               write_enable => sig_mem_write_mem,
               write_data   => sig_read_data_b_mem,
               addr_in      => sig_alu_result_mem(8 downto 0),
               data_out     => sig_data_mem_out_mem,
               test_finished=> done);
    
    tag_mem : tag_memory
    port map ( reset        => reset,
               clk          => clk,
               write_enable => sig_tag_write_mem,
               write_data   => sig_read_data_b_mem,
               addr_in      => sig_alu_result_mem(7 downto 0),
               data_out     => sig_data_tag_out_mem);
               
	 mem_wb_reg: mem_wb_register
	 port map ( reset => reset,
				   clk => clk,
				   tag_to_reg_mem => sig_tag_to_reg_mem,
				   mem_to_reg_mem => sig_mem_to_reg_mem,
				   reg_write_mem => sig_reg_write_mem,
					alu_result_mem => sig_alu_result_mem,
				   data_mem_out_mem => sig_data_mem_out_mem,
				   data_tag_out_mem => sig_data_tag_out_mem,
               op_code_mem => sig_op_code_mem,
               read_reg_a_mem => sig_read_reg_a_mem,
               read_reg_b_mem => sig_read_reg_b_mem,
					write_reg_mem => sig_write_reg_mem,
				   tag_to_reg_wb => sig_tag_to_reg_wb,
				   mem_to_reg_wb => sig_mem_to_reg_wb,
				   reg_write_wb => sig_reg_write_wb,
					alu_result_wb => sig_alu_result_wb,
				   data_mem_out_wb => sig_data_mem_out_wb,
				   data_tag_out_wb => sig_data_tag_out_wb,
				   write_reg_wb => sig_write_reg_wb,
               op_code_wb => sig_op_code_wb,
               read_reg_a_wb => sig_read_reg_a_wb,
               read_reg_b_wb => sig_read_reg_b_wb);		
					
    mux_data_to_mem : mux_2to1_16b 
    port map ( mux_select => sig_mem_to_reg_wb,
               data_a     => sig_alu_result_wb,
               data_b     => sig_data_mem_out_wb,
               data_out   => sig_data_out );
               
    mux_mem_to_reg : mux_2to1_16b 
    port map ( mux_select => sig_tag_to_reg_wb,
               data_a     => sig_data_out,
               data_b     => sig_data_tag_out_wb,
               data_out   => sig_write_data );
	 				
	 mux_pc_to_pc_jump : mux_2to1_8b
	 port map ( mux_select => sig_jump_condition_mem ,
               data_a     => sig_new_pc_branch,
					     data_b     => sig_alu_result_mem(7 downto 0),
					     data_out   => sig_new_pc);
					
	 mux_pc_to_pc_branch : mux_2to1_8b
	 port map ( mux_select => sig_branch_condition,
               data_a     => sig_next_pc,
					data_b     => sig_branch_addr_mem,
					data_out   => sig_new_pc_branch);
					
	 st_unit : stall_unit
	 port map ( op_code_mem      => sig_op_code_mem,     
		         branch_condition => sig_branch_condition,
		         op_code_ex       => sig_op_code_ex,
					op_code_id       => sig_insn_id(15 downto 12),
		         rt_id            => sig_insn_id(7 downto 4),
		         rs_id            => sig_insn_id(11 downto 8),
		         rt_ex            => sig_write_reg_ex,
		         flush            => sig_flush,
		         stall            => sig_stall);

					
end structural;
