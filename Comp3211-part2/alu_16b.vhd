---------------------------------------------------------------------------
-- adder_16b.vhd - 16-bit Adder Implementation
--
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity alu_16b is
    port ( src_a     : in  std_logic_vector(15 downto 0);
           src_b     : in  std_logic_vector(15 downto 0);
           control   : in  std_logic_vector(0 to 2);
           result    : out std_logic_vector(15 downto 0);
			  zero      : out std_logic;
           carry_out : out std_logic );
end alu_16b;

architecture behavioural of alu_16b is
constant ALU_ADD : std_logic_vector(0 to 2) := "000";
constant ALU_SUB : std_logic_vector(0 to 2) := "001";
constant ALU_AND : std_logic_vector(0 to 2) := "010";
constant ALU_OR  : std_logic_vector(0 to 2) := "011";
constant ALU_SLL : std_logic_vector(0 to 2) := "100";
constant ALU_SRL : std_logic_vector(0 to 2) := "101";
constant ALU_SLT : std_logic_vector(0 to 2) := "110";

signal sig_result : std_logic_vector(16 downto 0);

begin
	 alu_process: process (src_a, src_b, control) is
    variable n : integer; -- Used as shift offset
	 begin
       n := conv_integer(src_b(3 downto 0));
		 sig_result <= (others => '0');
		 case control is
          when ALU_ADD =>
			    sig_result <= ('0' & src_a) + ('0' & src_b);
		    when ALU_SUB =>
			    sig_result <= ('0' & src_a) - ('0' & src_b);
			 when ALU_AND =>
			    sig_result <= ('0' & src_a) and ('0' & src_b);
			 when ALU_OR  =>
			    sig_result <= ('0' & src_a) or  ('0' & src_b);
			 when ALU_SLL =>
             sig_result(15 downto n) <= src_a(15-n downto 0);
			 when ALU_SRL =>
			    sig_result(15-n downto 0) <= src_a(15 downto n);
			 when ALU_SLT =>
			    if src_a < src_b then
				    sig_result(0) <= '1';
				 end if;
			 when others =>
			   -- ALU should behave like ADD 
				sig_result <= ('0' & src_a) + ('0' & src_b);
		  end case;
    end process;
	 result     <= sig_result(15 downto 0) after 2 ns;
	 carry_out  <= sig_result(16) after 2 ns;
	 zero       <= '1' after 2 ns when src_a = "0000000000000000" else
	               '0' after 2 ns;
end behavioural;

