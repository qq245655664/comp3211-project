library ieee;
use ieee.std_logic_1164.all;

--entity forwarding_unit is
--    port ( ex_read_rs       : in std_logic_vector(3 downto 0);
--           ex_read_rt       : in std_logic_vector(3 downto 0);
--           mem_write_enable : in std_logic;
--           wb_write_enable  : in std_logic;
--           mem_write_reg    : in std_logic_vector(3 downto 0);
--           wb_write_reg     : in std_logic_vector(3 downto 0);
--           sel_alu_src_a    : out std_logic_vector(0 to 1); -- 00:origin 01:mem_result 10:write_data
--           sel_alu_src_b    : out std_logic_vector(0 to 1));
--end forwarding_unit;

entity forwarding_unit is
    port ( mem_wb_reg_write : in std_logic;
           mem_wb_reg_rd    : in std_logic_vector(3 downto 0);
           mem_wb_reg_rt    : in std_logic_vector(3 downto 0);
           mem_wb_reg_rs    : in std_logic_vector(3 downto 0);
           ex_mem_reg_write : in std_logic;
           ex_mem_reg_rd    : in std_logic_vector(3 downto 0);
           ex_mem_reg_rt    : in std_logic_vector(3 downto 0);
           ex_mem_reg_rs    : in std_logic_vector(3 downto 0);
           id_ex_reg_rs     : in std_logic_vector(3 downto 0);
           id_ex_reg_rt     : in std_logic_vector(3 downto 0);
           ex_mem_opcode    : in std_logic_vector(3 downto 0);
           mem_wb_opcode    : in std_logic_vector(3 downto 0);
           id_ex_opcode     : in std_logic_vector(3 downto 0);
           sel_alu_src_a    : out std_logic_vector(0 to 1); -- 00:origin 01:mem_result 10:write_data
           sel_alu_src_b    : out std_logic_vector(0 to 1);
			  sel_store_val    : out std_logic_vector(0 to 1);
			  use_extend       : in std_logic);
end forwarding_unit;

architecture behavioural of forwarding_unit is
constant OP_ADDI  : std_logic_vector(3 downto 0) := "0100";
constant OP_SLL   : std_logic_vector(3 downto 0) := "0110";
constant OP_SRL   : std_logic_vector(3 downto 0) := "0111";
constant OP_LOAD  : std_logic_vector(3 downto 0) := "0010";
constant OP_LOADT : std_logic_vector(3 downto 0) := "0101";
constant OP_STORE : std_logic_vector(3 downto 0) := "0011";
constant OP_STORET: std_logic_vector(3 downto 0) := "1101";
signal buf_mem_wb_reg_rd : std_logic_vector(3 downto 0);
signal buf_ex_mem_reg_rd : std_logic_vector(3 downto 0);

begin
    buf_mem_wb_reg_rd <= mem_wb_reg_rs when mem_wb_opcode = OP_ADDI else
                         mem_wb_reg_rt when mem_wb_opcode = OP_SLL  else
                         mem_wb_reg_rt when mem_wb_opcode = OP_SRL  else
							    mem_wb_reg_rt when mem_wb_opcode = OP_LOADT  else
								 mem_wb_reg_rt when mem_wb_opcode = OP_LOAD  else
                         mem_wb_reg_rd;
                         
    buf_ex_mem_reg_rd <= ex_mem_reg_rs when ex_mem_opcode = OP_ADDI else
                         ex_mem_reg_rt when ex_mem_opcode = OP_SLL  else
                         ex_mem_reg_rt when ex_mem_opcode = OP_SRL  else
								 ex_mem_reg_rt when ex_mem_opcode = OP_LOADT  else
								 ex_mem_reg_rt when ex_mem_opcode = OP_LOAD  else
                         ex_mem_reg_rd;
          
    sel_alu_src_a <= "01" after 1.5 ns when (ex_mem_reg_write = '1')
                                        and (buf_ex_mem_reg_rd /= "0000")
                                        and (buf_ex_mem_reg_rd = id_ex_reg_rs) else
                                        
                     "10" after 1.5 ns when (mem_wb_reg_write = '1')
                                        and (buf_mem_wb_reg_rd /= "0000")
                                        and (buf_ex_mem_reg_rd /= id_ex_reg_rs)
                                        and (buf_mem_wb_reg_rd = id_ex_reg_rs) else
                     "00" after 1.5 ns;

    sel_alu_src_b <= "01" after 1.5 ns when (ex_mem_reg_write = '1')
                                        and (buf_ex_mem_reg_rd /= "0000")
                                        and (buf_ex_mem_reg_rd = id_ex_reg_rt) 
                                        and (id_ex_opcode /= OP_ADDI) 
                                        and (id_ex_opcode /= OP_SLL) 
                                        and (id_ex_opcode /= OP_SRL)
                                        and (id_ex_opcode /= OP_STORE) 
													 and (id_ex_opcode /= OP_STORET) else
                                        
                     "10" after 1.5 ns when (mem_wb_reg_write = '1')
                                        and (mem_wb_reg_rd /= "0000")
                                        and (buf_ex_mem_reg_rd /= id_ex_reg_rt)
                                        and (buf_mem_wb_reg_rd = id_ex_reg_rt) 
                                        and (id_ex_opcode /= OP_ADDI) 
                                        and (id_ex_opcode /= OP_SLL) 
                                        and (id_ex_opcode /= OP_SRL)
                                        and (id_ex_opcode /= OP_STORE) 
													 and (id_ex_opcode /= OP_STORET)else
						   "11" after 1.5 ns when use_extend = '1' else
                     "00" after 1.5 ns;
	
	 sel_store_val <= "01" after 1.5 ns when (ex_mem_reg_write = '1')
                                        and (buf_mem_wb_reg_rd /= "0000")
                                        and (buf_ex_mem_reg_rd = id_ex_reg_rt)
													 and (id_ex_opcode = OP_STORE 
                                        or   id_ex_opcode = OP_STORET) else
						   "10" after 1.5 ns when (mem_wb_reg_write = '1')
                                        and (mem_wb_reg_rd /= "0000")
                                        and (buf_ex_mem_reg_rd /= id_ex_reg_rt)
                                        and (buf_mem_wb_reg_rd = id_ex_reg_rt)
                                        and (id_ex_opcode = OP_STORE 
                                        or   id_ex_opcode = OP_STORET) else
                     "00" after 1.5 ns;													 
end behavioural;                        
