library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_TEXTIO.ALL;
use STD.TEXTIO.ALL;

entity tag_memory is
    port ( reset        : in  std_logic;
           clk          : in  std_logic;
           write_enable : in  std_logic;
           write_data   : in  std_logic_vector(15 downto 0);
           addr_in      : in  std_logic_vector(7 downto 0);
           data_out     : out std_logic_vector(15 downto 0) );
end tag_memory;

architecture behavioral of tag_memory is

type tag_array is array(0 to 255) of std_logic_vector(15 downto 0);
signal sig_tag_mem : tag_array := ((others => (others => '0')));
file tag_file : text open read_mode is "tags.txt";

begin
    tag_process: process ( clk,
                           write_enable,
                           write_data,
                           addr_in ) is
  
    variable var_addr     : integer;
    variable line_buf     : line;
	 variable data_buf     : std_logic_vector(15 downto 0);
    begin
        var_addr := conv_integer(addr_in);
        
        if (reset = '1') then
				for i in 0 to 127 loop
					if (not endfile(tag_file)) then
						readline(tag_file,line_buf);
						read(line_buf,data_buf);
						sig_tag_mem(i) <= data_buf;
					end if;
				end loop;

        elsif (falling_edge(clk) and write_enable = '1') then
            -- memory writes on the falling clock edge
            sig_tag_mem(var_addr) <= write_data after 3 ns;
        end if;
       
        -- continuous read of the memory location given by var_addr 
        data_out <= sig_tag_mem(var_addr) after 3 ns;
    end process;
  
end behavioral;